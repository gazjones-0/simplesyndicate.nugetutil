# README #

SimpleSyndicate.NuGetUtil

### What is this repository for? ###

* Utilities for working with NuGet packages.

### How do I get started? ###

* Option 1 - build the application, and use it in your own projects
* Option 2 - install the NuGet package from https://www.nuget.org/packages/SimpleSyndicate.NuGetUtil, which will create a 'tools' directory in your solution root and install the application there

### Reporting issues ###

* Use the tracker at https://bitbucket.org/gazooka_g72/simplesyndicate.nugetutil/issues?status=new&status=open

### How do I use it? ###

```
#!text
Usage: SimpleSyndicate.NuGetUtil <command> [<args>]

Command summary:
currentversion               Outputs current version from .nuspec file
versionupdate                Increments version in .nuspec file and project
versionupdatenoreleasenotes  Similar to versionupdate
checkversionupdatearg        Check argument is valid for versionupdate
dependenciesupdate           Updates dependencies in .nuspec file
init                         Initialises a project 
reinit                       
prependline                  Prepends a line to a file
replace                      Replaces a string in a file
multireplace                 Replaces a string in multiple files
version                      Outputs NuGetUtil version

Command details:
currentversion [path]
  Outputs version in .nuspec file found in specified path.

  If path isn't specified, looks for .nuspec in current directory.

versionupdate <major|minor|patch|point|prerelease> [path]
  Increments version in .nuspec file found in specified path, then sets version
  in Properties\\AssemblyInfo.cs (in same path) and then for all folders in
  <path>\\.. and also updates release notes based on what's in the version
  history file it finds in a SandCastle project.

  If path isn't specified, looks for .nuspec in current directory.

  Patch and point can be used interchangeably, and mean the same thing.

  Version in .nuspec is expected to be in the form <major>.<minor>.<patch/point>
  or <major>.<minor>.<patch / point>-prerelease<nnn>.

  If prerelease is specified and there's no current prerelease number (i.e. it's
  x.y.z) then the current version is point incremented and -prerelease001
  appended (e.g. 1.0.2 would become 1.0.3-prerelease001). If there's already a
  prerelease number, it's incremented (e.g. 1.0.3-prerelease001 would become
  1.0.3-prerelease002).

  If patch / point version is specified and there's a prerelease part, the
  prerelease part is removed (e.g. 1.0.3-prerelease002 would become 1.0.3); if
  there's no prerelease part, point number is incremented.

  If minor version is specified, minor number is incremented, point version is
  set to 0 and any prerelease part is removed.

  If major version is specified, major number is incremented, minor and point
  numbers are set to 0 and any prerelease part is removed.

  If there's a second .nuspec file in the same directory that has same name
  but ends .Symbols.nupsec it will also be updated to match.

  There's two ways it tries to find version history -- first way is based on
  there being a separate solution for the help project, whilst second is the
  help in a project within the solution having version updated.

  First way uses name of last directory in <path>, but only up to first . in
  it, and looks two directories up for a directory named same with .Help after
  it (e.g. if name of last directory was SimpleSyndicate.Mvc it would look for
  SimpleSyndicate.Help). If directory's found, it's recursively searched for a
  file with same name as last directory in <path>, but with a .aml extension
  (e.g. if name of last directory was SimpleSyndicate.Mvc it would look for
  SimpleSyndicate.Mvc.aml).

  If first way doesn't find a file, all folders in parent directory of <path>
  will be recursively searched for VersionHistory.aml is found.

  In either case, release notes are based on what's in first
  //topic//developerConceptualDocument//section, and then the text in all the
  content//list//listItem within the section.

versionupdatenoreleasenotes <major|minor|patch|point|prerelease> [path]
  Identical to versionupdate except the release notes aren't changed.

checkversionupdatearg <major|minor|patch|point|prerelease>
  Checks whether the argument is valid for passing to versionupdate or
  versionupdatenoreleasenotes; helper command for other utilities to use to
  avoid them having to validate the argument themselves.

dependenciesupdate [path]
  Updates dependencies section in .nuspec file found in specified path.

  If path isn't specified, looks for .nuspec in current directory.

  If there's a second .nuspec file in the same directory that has same name
  but ends .Symbols.nupsec it will be updated to match.

init
[--path]
[--name]
[--nugetServer]
[--nuspecVersion] [--nuspecAuthors] [--nuspecOwners] [--nuspecProjectUrl]
[--nuspecRequireLicenseAcceptance] [--nuspecSummary] [--nuspecDescription]
[--nuspecReleaseNotes] [--nuspecCopyright] [--nuspecTags]
[--nogit]
[--gitPrefixTagWithProjectName]
[--vsXMLDoc] [--vsTreatWarningsAsErrors] [--vsCodeAnalysisOnRelease]
[--vsCodeAnalysisAllRulesRuleSet]
[--codeAnalysisDictionary]
[--styleCop] [--styleCopBuildIntegration] [--styleCopTreatErrorsAsBuildErrors]
  Initialises a Visual Studio project as a NuGet package, doing:
    Add standard .gitattributes .gitignore files
    Adds a template README.md, and includes it in the VS project
    Adds a tools folder with this executable in it
    Adds a template .nuspec, and includes it in the VS project
    Adds a nuget-push.cmd batch file for pushing the package to NuGet
    Removes the cruft Class1.cs or UnitTest1.cs files VS adds as default
    Makes the AssemblyInfo.cs match what's in the .nuspec

  The nuget-push.cmd batch file is designed to make NuGet releases easier,
  and does several things:
    Takes a parameter specifying the version increment (major, minor,
    point, prerelease)
    Increments the version in the project and .nuspec
    Builds the project in release mode
    Packs the build and pushes it to NuGet
    Commits all the changes in Git

  The standard convention is that the path refers to the root of a git
  repository, but if you're not using git, specify nogit with a non-blank
  value and the git aspects will not be included in nuget-push.cmd

  Similarly, the default is to add two standard .gitattributes and .gitignore
  files at the solution level (on assumption that the NuGet package will be a
  Git repository); if [nogit] is specified, these won't be added.

  If .gitattributes or .gitignore files already exist, they won't be
  overwritten (even when using reinit), on the assumption that there's a
  reason they already exist.

  The commit will be tagged with the version number, but by specifying
  [gitPrefixTagWithProjectName] you can have this prefixed with the project
  name, which is useful if you've multiple projects in one repository.

  Finally, if you want nuget-push to push the package to something other than
  the default public server, use [nugetServer] to specify the one to use.

  If no solution / project exists in the specified path, a template solution
  will be automatically created, using the specified [name]; if a solution /
  project already exists, it will be modified.

  The template created will be as Visual Studio would create, but certain
  aspects can be configured:
    [vsXmlDoc]
      Specifying this sets the project to generate XML documentation.
    [vsTreatWarningsAsErrors]
      Specifying this sets the project to treat warnings as errors.
    [vsCodeAnalysisOnRelease]
      Specifying this sets the project to do code analysis in release builds.
    [vsCodeAnalysisAllRulesRuleSet]
      Specifying this sets the project to use Microsoft All Rules.

  Some aspects of Code Analysis can be configured:
    [codeAnalysisDictionary]
      Specifying this adds a code analysis dictionary.

  Some aspects of StyleCop can be configured:
    [styleCop]
      Specifying this enables StyleCop in the solution.
    [styleCopBuildIntegration]
      Specifying this enables StyleCop build integration.
    [styleCopTreatErrorsAsBuildErrors]
      Specifying this treats StyleCop errors as build errors.

  If path isn't specified, looks for a solution / project in the current
  directory.

  If targeted at a project that already appears to have been initialised
  (e.g. there's already a .nuspec), this is considered an error -- you can
  use reinit in this case.

reinit
[--path]
[--name]
[--nugetServer]
[--nuspecVersion] [--nuspecAuthors] [--nuspecOwners] [--nuspecProjectUrl]
[--nuspecRequireLicenseAcceptance] [--nuspecSummary] [--nuspecDescription]
[--nuspecReleaseNotes] [--nuspecCopyright] [--nuspecTags]
[--nogit]
[--gitPrefixTagWithProjectName]
[--vsXMLDoc] [--vsTreatWarningsAsErrors] [--vsCodeAnalysisOnRelease]
[--vsCodeAnalysisAllRulesRuleSet]
[--codeAnalysisDictionary]
[--styleCop] [--styleCopBuildIntegration] [--styleCopTreatErrorsAsBuildErrors]
  Identical to init except that if targeted at a project that has already
  been initialised it will re-initialise it.

prependline <file> <line>
  Prepends the specified line to the specified file.
  If it already starts with specified line, it won't be prepended again.

replace <file> <oldValue> <newValue>
  Replaces the specified old string with the new string in the specified file.

multireplace <path> <oldValue> <newValue> [search pattern] [new file extension]
  Replaces the specified old string with the new string in files in the
  specfied path; the replace is recursive.

  If search pattern is specified, only files matching it will be replaced; if a
  new file extension is specified, the file will be renamed with the new
  extension replacing the existing one.

version
  Outputs the version of SimpleSyndicate.NuGetUtil.
```
