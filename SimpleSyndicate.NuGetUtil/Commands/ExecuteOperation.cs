﻿using System;

namespace SimpleSyndicate.NuGetUtil.Commands
{
	public static class ExecuteOperation
	{
		public static void Execute(Action operationToExecute)
		{
			if (operationToExecute == null)
			{
				throw new ArgumentNullException("operationToExecute");
			}
			int count = 0;
			while (true)
			{
				try
				{
					operationToExecute();
					return;
				}
				catch
				{
					count++;
					System.Threading.Thread.Sleep(1000 * 5);
					if (count > 5)
					{
						throw;
					}
				}
			}
		}
	}
}
