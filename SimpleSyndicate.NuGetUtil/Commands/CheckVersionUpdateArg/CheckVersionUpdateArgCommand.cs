﻿using System;
using System.Linq;

namespace SimpleSyndicate.NuGetUtil.Commands.CheckVersionUpdateArg
{
	public static class CheckVersionUpdateArgCommand
	{
		public static void Execute(string[] args)
		{
			if (args == null)
			{
				throw new ArgumentNullException("args");
			}
			if (CommandUtility.IsCommand(args, "CheckVersionUpdateArg"))
			{
				Program.CommandExecuted();
				if (args.Count() < 2)
				{
					Program.OutputError("No version component specified to check");
					return;
				}
				var versionUpdateComponentToCheck = Commands.VersionUpdate.VersionUpdateCommand.GetVersionUpdateComponent(args[1]);
				if (versionUpdateComponentToCheck != VersionUpdateComponent.Unknown)
				{
					Program.OutputInfo("Valid version component");
					return;
				}
				Program.OutputError("Invalid version component");
			}
		}
	}
}
