﻿using System.Linq;

namespace SimpleSyndicate.NuGetUtil.Commands
{
	public static class NoCommand
	{
		public static void Execute(string[] args)
		{
			if (args.Count() == 0)
			{
				Program.OutputUsage();
				Program.CommandExecuted();
			}
		}
	}
}
