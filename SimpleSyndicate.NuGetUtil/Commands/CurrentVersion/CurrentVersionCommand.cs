﻿
namespace SimpleSyndicate.NuGetUtil.Commands.CurrentVersion
{
	public static class CurrentVersionCommand
	{
		public static void Execute(string[] args)
		{
			if (CommandUtility.IsCommand(args, "CurrentVersion"))
			{
				Program.CommandExecuted();
				Output();
			}
		}

		public static void Output()
		{
			Output(System.Environment.CurrentDirectory);
		}

		public static void Output(string path)
		{
			var nuspecFile = NuspecFileFactory.Open(path, false);
			Program.OutputInfo(nuspecFile.Version);
		}
	}
}
