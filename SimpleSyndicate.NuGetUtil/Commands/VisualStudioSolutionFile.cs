﻿using System;
using System.Collections.Generic;
using System.IO;

namespace SimpleSyndicate.NuGetUtil.Commands
{
	public class VisualStudioSolutionFile
	{
		public string FileName { get; private set; }

		public string Path { get; private set; }

		private string originalLines = null;

		private List<string> lines = null;

		public VisualStudioSolutionFile(string path)
		{
			var actualPath = DetermineSolutionPath(path);
			FileName = DetermineFileName(actualPath);
			Path = Directory.GetParent(FileName).FullName;
		}

		private void LoadSolutionFile()
		{
			lines = new List<string>(File.ReadAllLines(FileName));
			originalLines = String.Join("\n", lines);
		}

		private void SaveSolutionFile()
		{
			if (lines == null)
			{
				throw new InvalidOperationException("Can't modify Visual Studio solution file as it was not loaded.");
			}
			if (String.Compare(originalLines, String.Join("\n", lines), StringComparison.Ordinal) != 0)
			{
				ExecuteOperation.Execute(() => { File.WriteAllLines(FileName, lines); });
			}
		}

		public void AddItemToSolutionItems(string itemName)
		{
			LoadSolutionFile();
			var index = AddSolutionItemsProjectIfRequired();
			index = AddPreProjectSectionIfRequired(index);
			AddItemToProjectSection(index, itemName);
			SaveSolutionFile();
		}

		private void AddItemToProjectSection(int index, string itemName)
		{
			while (lines[index].IndexOf("EndProjectSection", StringComparison.OrdinalIgnoreCase) < 0)
			{
				if (lines[index].IndexOf(itemName + " = " + itemName, StringComparison.OrdinalIgnoreCase) > 0)
				{
					return;
				}
				index++;
			}
			lines.Insert(index, "\t\t" + itemName + " = " + itemName);
		}

		private int AddSolutionItemsProjectIfRequired()
		{
			var index = ProjectExists("2150E333-8FDC-42A3-9474-1A3956D46DE8");
			if (index >= 0)
			{
				return index;
			}
			index = LastProject();
			lines.Insert(index, "EndProject");
			lines.Insert(index, "Project(\"{2150E333-8FDC-42A3-9474-1A3956D46DE8}\") = \"Solution Items\", \"Solution Items\", \"{" + Guid.NewGuid().ToString().ToUpperInvariant() + "}\"");
			return index;
		}

		private int ProjectExists(string guid)
		{
			int index = 0;
			while (index < lines.Count)
			{
				if (lines[index].IndexOf("Project(\"{" + guid + "}\")", StringComparison.OrdinalIgnoreCase) >= 0)
				{
					return index;
				}
				index++;
			}
			return -1;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "EndProject", Justification = "Literal is spelt correctly.")]
		private int LastProject()
		{
			int index = lines.Count - 1;
			while (index >= 0)
			{
				if (lines[index].IndexOf("EndProject", System.StringComparison.OrdinalIgnoreCase) >= 0)
				{
					return index + 1;
				}
				index--;
			}
			throw new InvalidOperationException("Can't find EndProject in Visual Studio solution file.");
		}

		private int AddPreProjectSectionIfRequired(int index)
		{
			var sectionIndex = ProjectSectionExists(index, "preProject");
			if (sectionIndex >= 0)
			{
				return sectionIndex;
			}
			sectionIndex = LastProjectSection(index);
			lines.Insert(sectionIndex, "\tEndProjectSection");
			lines.Insert(sectionIndex, "\tProjectSection(SolutionItems) = preProject");
			return sectionIndex;
		}

		private int ProjectSectionExists(int index, string name)
		{
			while (index < lines.Count)
			{
				if (lines[index].IndexOf(" = " + name, StringComparison.OrdinalIgnoreCase) >= 0)
				{
					return index;
				}
				if (lines[index].IndexOf("EndProject", StringComparison.OrdinalIgnoreCase) >= 0)
				{
					return -1;
				}
				index++;
			}
			return -1;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "EndProject", Justification = "Literal is spelt correctly.")]
		private int LastProjectSection(int index)
		{
			while (index < lines.Count)
			{
				if (lines[index].IndexOf("EndProject", System.StringComparison.OrdinalIgnoreCase) >= 0)
				{
					return index;
				}
				index++;
			}
			throw new InvalidOperationException("Can't find EndProject in Visual Studio solution file.");
		}

		private static string DetermineSolutionPath(string path)
		{
			foreach (var file in Directory.EnumerateFiles(path))
			{
				if (file.EndsWith(".sln", System.StringComparison.OrdinalIgnoreCase))
				{
					return path;
				}
			}
			var parent = Directory.GetParent(path).FullName;
			foreach (var file in Directory.EnumerateFiles(parent))
			{
				if (file.EndsWith(".sln", System.StringComparison.OrdinalIgnoreCase))
				{
					return parent;
				}
			}
			throw new FileNotFoundException("Couldn't find Visual Studio solution from " + path);
		}

		private static string DetermineFileName(string path)
		{
			if (File.Exists(path))
			{
				return path;
			}
			foreach (var file in Directory.EnumerateFiles(path))
			{
				if (file.EndsWith(".sln", System.StringComparison.OrdinalIgnoreCase))
				{
					return file;
				}
			}
			throw new FileNotFoundException("Couldn't find Visual Studio solution file in " + path);
		}
	}
}
