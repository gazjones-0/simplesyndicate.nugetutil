﻿
namespace SimpleSyndicate.NuGetUtil.Commands.Help
{
	public static class HelpCommand
	{
		public static void Execute(string[] args)
		{
			if (CommandUtility.IsCommand(args, "Help"))
			{
				Program.CommandExecuted();
				Program.OutputUsage();
			}
		}
	}
}
