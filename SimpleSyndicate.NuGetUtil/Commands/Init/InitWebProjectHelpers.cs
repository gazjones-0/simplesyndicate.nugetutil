﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace SimpleSyndicate.NuGetUtil.Commands.Init
{
	public static class InitWebProjectHelpers
	{
		public static void InitPackagesRepositoriesConfig()
		{
			if (!Directory.Exists(InitDetails.RepositoriesPath))
			{
				ExecuteOperation.Execute(() => { Directory.CreateDirectory(InitDetails.RepositoriesPath); });
			}
			var repositoriesConfig = Properties.Resources.VS2013Template_packages_repositories;
			repositoriesConfig = repositoriesConfig.Replace("$ProjectName$", InitDetails.VsProject.ProjectName);
			repositoriesConfig = repositoriesConfig.Replace("$PackagesConfigFileName$", InitDetails.PackagesConfigFileName);
			ExecuteOperation.Execute(() => { File.WriteAllText(InitDetails.RepositoriesConfigPath, repositoriesConfig); });
			Program.OutputInfo("Created " + InitDetails.RepositoriesConfigFileName);
		}

		public static void InitProjectAppConfig()
		{
			var appConfig = Properties.Resources.VS2013Template_Project_app;
			ExecuteOperation.Execute(() => { File.WriteAllText(InitDetails.AppConfigPath, appConfig); });
			Program.OutputInfo("Created " + InitDetails.AppConfigFileName);
		}

		public static void AddAppConfigToVisualStudioProject()
		{
			InitDetails.VsProject.AddItemBuildTypeNone(InitDetails.AppConfigFileName);
			Program.OutputInfo("Added " + InitDetails.AppConfigFileName + " to Visual Studio project.");
		}

		public static void InitProjectPackagesConfig()
		{
			var packagesConfig = Properties.Resources.VS2013Template_Project_packages;
			ExecuteOperation.Execute(() => { File.WriteAllText(InitDetails.PackagesConfigPath, packagesConfig); });
			Program.OutputInfo("Created " + InitDetails.PackagesConfigFileName);
		}

		public static void AddPackagesConfigToVisualStudioProject()
		{
			InitDetails.VsProject.AddItemBuildTypeNone(InitDetails.PackagesConfigFileName);
			Program.OutputInfo("Added " + InitDetails.PackagesConfigFileName + " to Visual Studio project.");
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "Any exception is considered an error.")]
		public static void InstallPackages()
		{
			// see if NuGet is available
			using (var process = new Process())
			{
				process.StartInfo.WorkingDirectory = InitDetails.RepositoriesPath;
				process.StartInfo.FileName = "nuget";
				process.StartInfo.UseShellExecute = false;
				process.StartInfo.RedirectStandardOutput = true;
				process.StartInfo.CreateNoWindow = true;
				string stdout = null;
				bool nugetFound = false;
				try
				{
					process.Start();
					stdout = process.StandardOutput.ReadToEnd();
					process.WaitForExit();
					if (stdout.StartsWith("NuGet", StringComparison.OrdinalIgnoreCase))
					{
						nugetFound = true;
					}
				}
				catch (Exception e)
				{
					Program.OutputError("Caught exception whilst checking for nuget\r\n" + e.ToString());
				}
				if (!nugetFound)
				{
					Program.OutputError("NuGet not installed.");
					return;
				}
			}

			// use NuGet to install the packages
			using (var process = new Process())
			{
				process.StartInfo.WorkingDirectory = InitDetails.RepositoriesPath;
				process.StartInfo.FileName = "nuget";
				process.StartInfo.Arguments = "install \"" + InitDetails.PackagesConfigPath + "\"";
				process.StartInfo.UseShellExecute = false;
				process.StartInfo.RedirectStandardError = true;
				process.StartInfo.RedirectStandardOutput = true;
				process.StartInfo.CreateNoWindow = true;
				string stderr = null;
				string stdout = null;
				string exception = null;
				try
				{
					process.Start();
					stderr = process.StandardError.ReadToEnd();
					stdout = process.StandardOutput.ReadToEnd();
					process.WaitForExit();
				}
				catch (Exception e)
				{
					exception = "Caught exception whilst trying to install packages\r\n" + e.ToString();
				}
				if (!String.IsNullOrWhiteSpace(stdout))
				{
					Program.OutputInfo(stdout);
				}
				if (!String.IsNullOrWhiteSpace(stderr))
				{
					Program.OutputInfo(stderr);
				}
				if (!String.IsNullOrWhiteSpace(exception))
				{
					Program.OutputError(exception);
				}
				Program.OutputInfo("Installed packages");
			}
		}

		public static void AddPackageReferencesToVisualStudioProject()
		{
			DirectoryInfo packages = new DirectoryInfo(InitDetails.PackagesPath);
			foreach (var libDir in packages.GetDirectories("lib", SearchOption.AllDirectories))
			{
				string assemblyPath = null;
				switch (libDir.GetDirectories().Count())
				{
					case 0:
						assemblyPath = libDir.FullName;
						break;
					case 1:
						assemblyPath = libDir.GetDirectories().First().FullName;
						break;
					default:
						var possible = libDir.GetDirectories("net45");
						if (possible.Count() == 1)
						{
							assemblyPath = possible.First().FullName;
						}
						else
						{
							possible = libDir.GetDirectories("net40");
							if (possible.Count() == 1)
							{
								assemblyPath = possible.First().FullName;
							}
							else
							{
								possible = libDir.GetDirectories("net35");
								if (possible.Count() == 1)
								{
									assemblyPath = possible.First().FullName;
								}
								else
								{
									possible = libDir.GetDirectories("net20");
									if (possible.Count() == 1)
									{
										assemblyPath = possible.First().FullName;
									}
								}
							}
						}
						break;
				}
				if (assemblyPath != null)
				{
					foreach (var assembly in new DirectoryInfo(assemblyPath).GetFiles("*.dll"))
					{
						var name = System.Reflection.AssemblyName.GetAssemblyName(assembly.FullName);
						var include = name.FullName + ", processorArchitecture=" + name.ProcessorArchitecture.ToString();
						var hintPath = assembly.FullName.Replace(InitDetails.PackagesPath, "..\\packages");
						InitDetails.VsProject.AddReference(include, hintPath, true);
						Program.OutputInfo("Added reference to " + name.Name + " to Visual Studio project.");
					}
				}
			}
		}

		public static void SetDebugOutputPathInVisualStudioProject()
		{
			InitDetails.VsProject.AddDebugProperty("OutputPath", "bin\\");
			Program.OutputInfo("Updated output path to bin\\ for debug builds.");
		}

		public static void InitNuspec()
		{
			InitHelpers.InitNuspec(Properties.Resources.NuGetCustomPackage_Package);
		}

		public static void AddNuspecToVisualStudioProject()
		{
			InitDetails.VsProject.AddItemBuildTypeContent(InitDetails.VsProject.ProjectName + ".nuspec");
			Program.OutputInfo("Added " + InitDetails.VsProject.ProjectName + ".nuspec to Visual Studio project.");
		}

		public static void InitSymbolsNuspec()
		{
			InitHelpers.InitNuspec(InitDetails.SymbolsNuspecPath, Properties.Resources.NuGetCustomPackage_Package_Symbols);
		}

		public static void AddSymbolsNuspecToVisualStudioProject()
		{
			InitDetails.VsProject.AddItemBuildTypeContent(InitDetails.VsProject.ProjectName + ".Symbols.nuspec");
			Program.OutputInfo("Added " + InitDetails.VsProject.ProjectName + ".Symbols.nuspec to Visual Studio project.");
		}

		public static void InitNuGetPush(string[] args)
		{
			InitHelpers.InitNuGetPush(args, Properties.Resources.NuGetCustomPackage_nuget_push_push_UpdateDependencies, Properties.Resources.NuGetCustomPackage_nuget_push_push_BuildAndPush);
		}

		public static void AddNuGetPushToVisualStudioProject()
		{
			InitDetails.VsProject.AddItemBuildTypeContent(InitDetails.NugetPushFileName);
			Program.OutputInfo("Added " + InitDetails.NugetPushFileName + " to Visual Studio project.");
		}

		public static void InitReadme()
		{
			ExecuteOperation.Execute(() => { File.WriteAllText(InitDetails.ProjectReadmePath, Properties.Resources.VS2013Template_Project_readme); });
			Replace.ReplaceCommand.ReplaceString(InitDetails.ProjectReadmePath, "$project$", InitDetails.VsProject.ProjectName);
			Program.OutputInfo("Created " + InitDetails.ProjectReadmeFileName);
		}

		public static void AddReadmeToVisualStudioProject()
		{
			InitDetails.VsProject.AddItemBuildTypeContent(InitDetails.ProjectReadmeFileName);
			Program.OutputInfo("Added " + InitDetails.ProjectReadmeFileName + " to Visual Studio project.");
		}

		public static void InitWebConfig()
		{
			ExecuteOperation.Execute(() => { File.WriteAllText(InitDetails.WebConfigPath, Properties.Resources.VS2013Template_WebProject_Web); });
			Program.OutputInfo("Created " + InitDetails.WebConfigFileName);
		}

		public static void AddWebConfigToVisualStudioProject()
		{
			InitDetails.VsProject.AddItemBuildTypeContent(InitDetails.WebConfigFileName);
			Program.OutputInfo("Added " + InitDetails.WebConfigFileName + " to Visual Studio project.");
		}

		public static void InitWebConfigDebug()
		{
			ExecuteOperation.Execute(() => { File.WriteAllText(InitDetails.WebConfigDebugPath, Properties.Resources.VS2013Template_WebProject_Web_Debug); });
			Program.OutputInfo("Created " + InitDetails.WebConfigDebugFileName);
		}

		public static void AddWebConfigDebugToVisualStudioProject()
		{
			InitDetails.VsProject.AddItemBuildTypeNone(InitDetails.WebConfigDebugFileName, null, "Web.config");
			Program.OutputInfo("Added " + InitDetails.WebConfigDebugFileName + " to Visual Studio project.");
		}

		public static void InitWebConfigRelease()
		{
			ExecuteOperation.Execute(() => { File.WriteAllText(InitDetails.WebConfigReleasePath, Properties.Resources.VS2013Template_WebProject_Web_Release); });
			Program.OutputInfo("Created " + InitDetails.WebConfigReleaseFileName);
		}

		public static void AddWebConfigReleaseToVisualStudioProject()
		{
			InitDetails.VsProject.AddItemBuildTypeNone(InitDetails.WebConfigReleaseFileName, null, "Web.config");
			Program.OutputInfo("Added " + InitDetails.WebConfigReleaseFileName + " to Visual Studio project.");
		}
	}
}
