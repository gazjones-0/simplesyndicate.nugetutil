﻿using System;
using System.Globalization;
using System.IO;

namespace SimpleSyndicate.NuGetUtil.Commands.Init
{
	public static class InitDetails
	{
		private static VisualStudioProjectFile vsProject = null;

		private static VisualStudioProjectFile testsVsProject = null;

		private static string gitattributesFileName = ".gitattributes";
		private static string gitattributesPath = null;
		private static string gitignoreFileName = ".gitignore";
		private static string gitignorePath = null;

		private static string toolsPath = null;
		private static string toolsSimpleSyndicateNugetUtilFileName = "SimpleSyndicate.NuGetUtil.exe";
		private static string toolsSimpleSyndicateNugetUtilPath = null;

		private static string readmeFileName = "README.md";
		private static string readmePath = null;

		private static string documentationLink = "?";
		private static string trackerLink = "?";

		private static string nugetPushFileName = "nuget-push.cmd";
		private static string nugetPushPath = null;

		private static string nuspecPath = null;
		private static string nuspecVersion = null;
		private static string nuspecAuthors = null;
		private static string nuspecOwners = null;
		private static string nuspecProjectUrl = null;
		private static string nuspecRequireLicenseAcceptance = null;
		private static string nuspecSummary = null;
		private static string nuspecDescription = null;
		private static string nuspecReleaseNotes = null;
		private static string nuspecCopyright = null;
		private static string nuspecTags = null;

		private static string symbolsNuspecPath = null;

		private static string assemblyInfoCompany = null;
		private static string assemblyInfoConfiguration = null;
		private static string assemblyInfoCopyright = null;
		private static string assemblyInfoCulture = null;
		private static string assemblyInfoDescription = null;
		private static string assemblyInfoTrademark = null;
		private static bool assemblyInfoCLSCompliant = false;

		private static string repositoriesPath = null;
		private static string repositoriesConfigFileName = "repositories.config";
		private static string repositoriesConfigPath = null;

		private static string appConfigFileName = "app.config";
		private static string appConfigPath = null;

		private static string packagesPath = null;
		private static string packagesConfigFileName = "packages.config";
		private static string packagesConfigPath = null;

		private static string webConfigFileName = "Web.config";
		private static string webConfigPath = null;
		private static string webConfigDebugFileName = "Web.Debug.config";
		private static string webConfigDebugPath = null;
		private static string webConfigReleaseFileName = "Web.Release.config";
		private static string webConfigReleasePath = null;

		private static string projectReadmeFileName = "readme.txt";
		private static string projectReadmePath = null;

		private static bool codeAnalysisDictionary = false;

		private static bool styleCop = false;
		private static bool styleCopBuildIntegration = false;
		private static bool styleCopTreatErrorsAsBuildErrors = false;

		public static VisualStudioProjectFile VsProject { get { return vsProject; } }

		public static VisualStudioProjectFile TestsVsProject { get { return testsVsProject; } }

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Gitattributes", Justification = "Identifier is spelt correctly.")]
		public static string GitattributesFileName { get { return gitattributesFileName; } }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Gitattributes", Justification = "Identifier is spelt correctly.")]
		public static string GitattributesPath { get { return gitattributesPath; } }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Gitignore", Justification = "Identifier is spelt correctly.")]
		public static string GitignoreFileName { get { return gitignoreFileName; } }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Gitignore", Justification = "Identifier is spelt correctly.")]
		public static string GitignorePath { get { return gitignorePath; } }

		public static string ToolsPath { get { return toolsPath; } }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "NugetUtil", Justification = "Casing is correct."), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Nuget", Justification = "Identifier is spelt correctly.")]
		public static string ToolsSimpleSyndicateNugetUtilFileName { get { return toolsSimpleSyndicateNugetUtilFileName; } }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "NugetUtil", Justification = "Casing is correct."), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Nuget", Justification = "Identifier is spelt correctly.")]
		public static string ToolsSimpleSyndicateNugetUtilPath { get { return toolsSimpleSyndicateNugetUtilPath; } }

		public static string ReadmeFileName { get { return readmeFileName; } }
		public static string ReadmePath { get { return readmePath; } }

		public static string DocumentationLink { get { return documentationLink; } }
		public static string TrackerLink { get { return trackerLink; } }

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Nuget", Justification = "Identifier is spelt correctly.")]
		public static string NugetPushFileName { get { return nugetPushFileName; } }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Nuget", Justification = "Identifier is spelt correctly.")]
		public static string NugetPushPath { get { return nugetPushPath; } }

		public static string NuspecPath { get { return nuspecPath; } }
		public static string NuspecVersion { get { return nuspecVersion; } }
		public static string NuspecAuthors { get { return nuspecAuthors; } }
		public static string NuspecOwners { get { return nuspecOwners; } }
		public static Uri NuspecProjectUrl { get { return !String.IsNullOrWhiteSpace(nuspecProjectUrl) && System.Uri.IsWellFormedUriString(nuspecProjectUrl, UriKind.Absolute) ? new Uri(nuspecProjectUrl) : null; } }
		public static string NuspecRequireLicenseAcceptance { get { return nuspecRequireLicenseAcceptance; } }
		public static string NuspecSummary { get { return nuspecSummary; } }
		public static string NuspecDescription { get { return nuspecDescription; } }
		public static string NuspecReleaseNotes { get { return nuspecReleaseNotes; } }
		public static string NuspecCopyright { get { return nuspecCopyright; } }
		public static string NuspecTags { get { return nuspecTags; } }

		public static string SymbolsNuspecPath { get { return symbolsNuspecPath; } }

		public static string AssemblyInfoCompany { get { return assemblyInfoCompany; } }
		public static string AssemblyInfoConfiguration { get { return assemblyInfoConfiguration; } }
		public static string AssemblyInfoCopyright { get { return assemblyInfoCopyright; } }
		public static string AssemblyInfoCulture { get { return assemblyInfoCulture; } }
		public static string AssemblyInfoDescription { get { return assemblyInfoDescription; } }
		public static string AssemblyInfoTrademark { get { return assemblyInfoTrademark; } }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "CLS", Justification = "Identifier matches .net framework.")]
		public static bool AssemblyInfoCLSCompliant { get { return assemblyInfoCLSCompliant; } }

		public static string RepositoriesPath { get { return repositoriesPath; } }
		public static string RepositoriesConfigFileName { get { return repositoriesConfigFileName; } }
		public static string RepositoriesConfigPath { get { return repositoriesConfigPath; } }

		public static string AppConfigFileName { get { return appConfigFileName; } }
		public static string AppConfigPath { get { return appConfigPath; } }

		public static string PackagesPath { get { return packagesPath; } }
		public static string PackagesConfigFileName { get { return packagesConfigFileName; } }
		public static string PackagesConfigPath { get { return packagesConfigPath; } }

		public static string WebConfigFileName { get { return webConfigFileName; } }
		public static string WebConfigPath { get { return webConfigPath; } }
		public static string WebConfigDebugFileName { get { return webConfigDebugFileName; } }
		public static string WebConfigDebugPath { get { return webConfigDebugPath; } }
		public static string WebConfigReleaseFileName { get { return webConfigReleaseFileName; } }
		public static string WebConfigReleasePath { get { return webConfigReleasePath; } }

		public static string ProjectReadmeFileName { get { return projectReadmeFileName; } }
		public static string ProjectReadmePath { get { return projectReadmePath; } }

		public static bool CodeAnalysisDictionary { get { return codeAnalysisDictionary; } }

		public static bool StyleCop { get { return styleCop; } }
		public static bool StyleCopBuildIntegration { get { return styleCopBuildIntegration; } }
		public static bool StyleCopTreatErrorsAsBuildErrors { get { return styleCopTreatErrorsAsBuildErrors; } }

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase", Justification = "Project names lower-cased in BitBucket URLs by convention.")]
		public static void DetermineDetails(string[] args, string projectPath, string testsProjectPath)
		{
			vsProject = new VisualStudioProjectFile(projectPath);

			testsVsProject = new VisualStudioProjectFile(testsProjectPath);

			gitattributesPath = Path.Combine(vsProject.SolutionFile.Path, gitattributesFileName);
			gitignorePath = Path.Combine(vsProject.SolutionFile.Path, gitignoreFileName);

			toolsPath = Path.Combine(vsProject.SolutionFile.Path, "tools");
			toolsSimpleSyndicateNugetUtilPath = Path.Combine(toolsPath, toolsSimpleSyndicateNugetUtilFileName);

			readmePath = Path.Combine(vsProject.SolutionFile.Path, readmeFileName);

			if (!!String.IsNullOrWhiteSpace(CommandUtility.GetArgument(args, "ss")))
			{
				documentationLink = "http://gazooka_g72.bitbucket.org/SimpleSyndicate";
				trackerLink = "https://bitbucket.org/gazooka_g72/$project.lowercase$/issues?status=new&status=open";
			}

			nugetPushPath = Path.Combine(vsProject.Path, nugetPushFileName);

			nuspecPath = vsProject.FileName.Replace(".csproj", ".nuspec");
			nuspecVersion = CommandUtility.GetArgument(args, "nuspecVersion", "0.0.0");
			nuspecAuthors = CommandUtility.GetArgument(args, "nuspecAuthors", !String.IsNullOrWhiteSpace(CommandUtility.GetArgument(args, "ss")) ? "simpleSyndicate" : "?");
			nuspecOwners = CommandUtility.GetArgument(args, "nuspecOwners", nuspecAuthors);
			nuspecProjectUrl = CommandUtility.GetArgument(args, "nuspecProjectUrl", !String.IsNullOrWhiteSpace(CommandUtility.GetArgument(args, "ss")) ? "https://bitbucket.org/gazooka_g72/" + vsProject.ProjectName.ToLowerInvariant() : "?");
			if ((CommandUtility.GetArgument(args, "nuspecAuthors") != null)
				|| (CommandUtility.GetArgument(args, "nuspecOwners") != null))
			{
				nuspecProjectUrl = CommandUtility.GetArgument(args, "nuspecProjectUrl", "?");
			}
			nuspecRequireLicenseAcceptance = CommandUtility.GetArgument(args, "nuspecRequireLicenseAcceptance", "false");
			nuspecSummary = CommandUtility.GetArgument(args, "nuspecSummary", "?");
			nuspecDescription = CommandUtility.GetArgument(args, "nuspecDescription", "?");
			nuspecReleaseNotes = CommandUtility.GetArgument(args, "nuspecReleaseNotes", "Initial release.");
			nuspecCopyright = CommandUtility.GetArgument(args, "nuspecCopyright", "Copyright © " + nuspecOwners + " " + DateTime.Now.Year.ToString(CultureInfo.InvariantCulture));
			nuspecTags = CommandUtility.GetArgument(args, "nuspecTags", "?");

			symbolsNuspecPath = vsProject.FileName.Replace(".csproj", ".Symbols.nuspec");

			assemblyInfoCompany = nuspecOwners;
			assemblyInfoConfiguration = "";
			assemblyInfoCopyright = nuspecCopyright;
			assemblyInfoCulture = "";
			assemblyInfoDescription = "";
			assemblyInfoTrademark = "";
			assemblyInfoCLSCompliant = true;

			repositoriesPath = Path.Combine(vsProject.SolutionFile.Path, "packages");
			repositoriesConfigPath = Path.Combine(repositoriesPath, repositoriesConfigFileName);

			appConfigPath = Path.Combine(vsProject.Path, appConfigFileName);

			packagesPath = Path.Combine(vsProject.SolutionFile.Path, "packages");
			packagesConfigPath = Path.Combine(vsProject.Path, packagesConfigFileName);

			webConfigPath = Path.Combine(vsProject.Path, webConfigFileName);
			webConfigDebugPath = Path.Combine(vsProject.Path, webConfigDebugFileName);
			webConfigReleasePath = Path.Combine(vsProject.Path, webConfigReleaseFileName);

			projectReadmePath = Path.Combine(vsProject.Path, projectReadmeFileName);

			if ((!String.IsNullOrWhiteSpace(CommandUtility.GetArgument(args, "codeAnalysisDictionary")))
				|| (!String.IsNullOrWhiteSpace(CommandUtility.GetArgument(args, "ss"))))
			{
				codeAnalysisDictionary = true;
			}

			if ((!String.IsNullOrWhiteSpace(CommandUtility.GetArgument(args, "styleCop")))
				|| (!String.IsNullOrWhiteSpace(CommandUtility.GetArgument(args, "ss"))))
			{
				styleCop = true;
			}

			if ((!String.IsNullOrWhiteSpace(CommandUtility.GetArgument(args, "styleCopBuildIntegration")))
				|| (!String.IsNullOrWhiteSpace(CommandUtility.GetArgument(args, "ss"))))
			{
				styleCopBuildIntegration = true;
			}

			if ((!String.IsNullOrWhiteSpace(CommandUtility.GetArgument(args, "styleCopTreatErrorsAsBuildErrors")))
				|| (!String.IsNullOrWhiteSpace(CommandUtility.GetArgument(args, "ss"))))
			{
				styleCopTreatErrorsAsBuildErrors = true;
			}
		}
	}
}
