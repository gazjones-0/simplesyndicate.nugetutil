﻿using System;
using System.IO;

namespace SimpleSyndicate.NuGetUtil.Commands.Init
{
	public static class VisualStudioDetails
	{
		private static string rootPath = null;

		private static string solutionPath = null;
		private static string solutionFileName = null;

		private static string projectRoot = null;
		private static string projectPath = null;
		private static string projectName = null;
		private static string projectGuid = null;
		private static string projectAssemblyGuid = null;
		private static bool projectIsWebProject = false;
		private static bool projectXmlDocumentationFile = false;
		private static bool projectTreatWarningsAsErrors = false;
		private static bool projectRunCodeAnalysisOnRelease = false;
		private static bool projectCodeAnalysisAllRulesRuleSet = false;

		private static string projectClass1Path = null;

		private static string projectPropertiesPath = null;
		private static string projectPropertiesAssemblyInfoPath = null;

		private static string testsProjectRoot = null;
		private static string testsProjectPath = null;
		private static string testsProjectName = null;
		private static string testsProjectGuid = null;
		private static string testsProjectAssemblyGuid = null;

		private static string testsProjectUnitTest1Path = null;

		private static string testsProjectPropertiesPath = null;
		private static string testsProjectPropertiesAssemblyInfoPath = null;

		public static string RootPath { get { return rootPath; } }

		public static string SolutionPath { get { return solutionPath; } }
		public static string SolutionFileName { get { return solutionFileName; } }

		public static string ProjectRoot { get { return projectRoot; } }
		public static string ProjectPath { get { return projectPath; } }
		public static string ProjectName { get { return projectName; } }
		public static string ProjectGuid { get { return projectGuid; } }
		public static string ProjectAssemblyGuid { get { return projectAssemblyGuid; } }
		public static bool ProjectIsWebProject { get { return projectIsWebProject; } }
		public static bool ProjectXmlDocumentationFile { get { return projectXmlDocumentationFile; } }
		public static bool ProjectTreatWarningsAsErrors { get { return projectTreatWarningsAsErrors; } }
		public static bool ProjectRunCodeAnalysisOnRelease { get { return projectRunCodeAnalysisOnRelease; } }
		public static bool ProjectCodeAnalysisAllRulesRuleSet { get { return projectCodeAnalysisAllRulesRuleSet; } }

		public static string ProjectClass1Path { get { return projectClass1Path; } }

		public static string ProjectPropertiesPath { get { return projectPropertiesPath; } }
		public static string ProjectPropertiesAssemblyInfoPath { get { return projectPropertiesAssemblyInfoPath; } }

		public static string TestsProjectRoot { get { return testsProjectRoot; } }
		public static string TestsProjectPath { get { return testsProjectPath; } }
		public static string TestsProjectName { get { return testsProjectName; } }
		public static string TestsProjectGuid { get { return testsProjectGuid; } }
		public static string TestsProjectAssemblyGuid { get { return testsProjectAssemblyGuid; } }

		public static string TestsProjectUnitTest1Path { get { return testsProjectUnitTest1Path; } }

		public static string TestsProjectPropertiesPath { get { return testsProjectPropertiesPath; } }
		public static string TestsProjectPropertiesAssemblyInfoPath { get { return testsProjectPropertiesAssemblyInfoPath; } }

		public static void DetermineDetails(string[] args, string path, string name)
		{
			rootPath = path;
			solutionFileName = Path.Combine(name + ".sln");
			solutionPath = Path.Combine(path, solutionFileName);

			projectName = name;
			projectRoot = Path.Combine(rootPath, projectName);
			projectPath = Path.Combine(projectRoot, projectName + ".csproj");
			projectGuid = Guid.NewGuid().ToString().ToUpperInvariant();
			projectAssemblyGuid = Guid.NewGuid().ToString();
			if (!String.IsNullOrWhiteSpace(CommandUtility.GetArgument(args, "webproject")))
			{
				projectIsWebProject = true;
			}
			if ((!String.IsNullOrWhiteSpace(CommandUtility.GetArgument(args, "ss")))
				|| (!String.IsNullOrWhiteSpace(CommandUtility.GetArgument(args, "vsXMLDoc"))))
			{
				projectXmlDocumentationFile = true;
			}
			if ((!String.IsNullOrWhiteSpace(CommandUtility.GetArgument(args, "ss")))
				|| (!String.IsNullOrWhiteSpace(CommandUtility.GetArgument(args, "vsTreatWarningsAsErrors"))))
			{
				projectTreatWarningsAsErrors = true;
			}
			if ((!String.IsNullOrWhiteSpace(CommandUtility.GetArgument(args, "ss")))
				|| (!String.IsNullOrWhiteSpace(CommandUtility.GetArgument(args, "vsCodeAnalysisOnRelease"))))
			{
				projectRunCodeAnalysisOnRelease = true;
			}
			if ((!String.IsNullOrWhiteSpace(CommandUtility.GetArgument(args, "ss")))
				|| (!String.IsNullOrWhiteSpace(CommandUtility.GetArgument(args, "vsCodeAnalysisAllRulesRuleSet"))))
			{
				projectCodeAnalysisAllRulesRuleSet = true;
			}

			projectClass1Path = Path.Combine(projectRoot, "Class1.cs");

			projectPropertiesPath = Path.Combine(projectRoot, "Properties");
			projectPropertiesAssemblyInfoPath = Path.Combine(projectPropertiesPath, "AssemblyInfo.cs");

			testsProjectName = name + ".Tests";
			testsProjectRoot = Path.Combine(rootPath, testsProjectName);
			testsProjectPath = Path.Combine(testsProjectRoot, testsProjectName + ".csproj");
			testsProjectGuid = Guid.NewGuid().ToString().ToUpperInvariant();
			testsProjectAssemblyGuid = Guid.NewGuid().ToString();

			testsProjectUnitTest1Path = Path.Combine(testsProjectRoot, "UnitTest1.cs");

			testsProjectPropertiesPath = Path.Combine(testsProjectRoot, "Properties");
			testsProjectPropertiesAssemblyInfoPath = Path.Combine(testsProjectPropertiesPath, "AssemblyInfo.cs");
		}
	}
}
