﻿using System;

namespace SimpleSyndicate.NuGetUtil.Commands.Init
{
	public static class InitCommand
	{
		public static void Execute(string[] args)
		{
			if (args == null)
			{
				throw new ArgumentNullException("args");
			}
			if (CommandUtility.IsCommand(args, "Init") || CommandUtility.IsCommand(args, "ReInit"))
			{
				Program.CommandExecuted();
				if (!CommandUtility.AreArgumentsValid(args))
				{
					Program.OutputError("Invalid arguments specified.");
					return;
				}
				var projectPath = VisualStudioHelpers.DetermineProjectPath(args);
				if (String.IsNullOrWhiteSpace(projectPath))
				{
					Program.OutputError("No Visual Studio project found.");
					return;
				}
				var testsProjectPath = VisualStudioHelpers.DetermineTestsProjectPath(args);
				if (String.IsNullOrWhiteSpace(testsProjectPath))
				{
					Program.OutputInfo("No Visual Studio tests project found, assuming no tests project.");
					return;
				}
				Init(args, projectPath, testsProjectPath, CommandUtility.IsCommand(args, "ReInit"));
			}
		}

		private static void Init(string[] args, string projectPath, string testsProjectPath, bool reInit)
		{
			InitDetails.DetermineDetails(args, projectPath, testsProjectPath);
			if (!reInit && !InitHelpers.PreCheck())
			{
				return;
			}
			if (String.IsNullOrWhiteSpace(CommandUtility.GetArgument(args, "nogit")))
			{
				InitHelpers.InitGitAttributes();
				InitHelpers.InitGitIgnore();
			}
			InitHelpers.InitVisualStudioProjectFile();
			InitHelpers.InitReadme();
			InitHelpers.AddReadmeToVisualStudioSolution();
			InitHelpers.InitTools();
			InitHelpers.InitNuspec();
			InitHelpers.AddNuspecToVisualStudioProject();
			InitHelpers.InitNuGetPush(args);
			InitHelpers.AddNuGetPushToVisualStudioProject();
			InitHelpers.InitGlobalSuppressions();
			InitHelpers.AddGlobalSuppressionsToVisualStudioProject();
			InitHelpers.InitCodeAnalysisDictionary();
			InitHelpers.AddCodeAnalysisDictionaryToVisualStudioProject();
			InitHelpers.InitStyleCopSettings();
			InitHelpers.AddStyleCopSettingsToVisualStudioSolution();
			InitHelpers.RemoveClass1FromVisualStudioProject();
			InitHelpers.RemoveUnitTest1FromVisualStudioTestsProject();
			InitHelpers.InitAssemblyInfo();

			if (!String.IsNullOrWhiteSpace(CommandUtility.GetArgument(args, "webproject")))
			{
				InitWebProjectHelpers.InitPackagesRepositoriesConfig();
				InitWebProjectHelpers.InitProjectAppConfig();
				InitWebProjectHelpers.AddAppConfigToVisualStudioProject();
				InitWebProjectHelpers.InitProjectPackagesConfig();
				InitWebProjectHelpers.AddPackagesConfigToVisualStudioProject();
				InitWebProjectHelpers.InstallPackages();
				InitWebProjectHelpers.AddPackageReferencesToVisualStudioProject();
				InitWebProjectHelpers.SetDebugOutputPathInVisualStudioProject();
				InitWebProjectHelpers.InitNuspec();
				InitWebProjectHelpers.AddNuspecToVisualStudioProject();
				InitWebProjectHelpers.InitSymbolsNuspec();
				InitWebProjectHelpers.AddSymbolsNuspecToVisualStudioProject();
				InitWebProjectHelpers.InitNuGetPush(args);
				InitWebProjectHelpers.AddNuGetPushToVisualStudioProject();
				InitWebProjectHelpers.InitReadme();
				InitWebProjectHelpers.AddReadmeToVisualStudioProject();
				InitWebProjectHelpers.InitWebConfig();
				InitWebProjectHelpers.AddWebConfigToVisualStudioProject();
				InitWebProjectHelpers.InitWebConfigDebug();
				InitWebProjectHelpers.AddWebConfigDebugToVisualStudioProject();
				InitWebProjectHelpers.InitWebConfigRelease();
				InitWebProjectHelpers.AddWebConfigReleaseToVisualStudioProject();
			}
		}
	}
}
