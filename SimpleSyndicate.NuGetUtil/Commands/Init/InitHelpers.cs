﻿using System;
using System.IO;

namespace SimpleSyndicate.NuGetUtil.Commands.Init
{
	public static class InitHelpers
	{
		public static bool PreCheck()
		{
			if (File.Exists(InitDetails.ReadmePath)
				|| File.Exists(InitDetails.NugetPushPath)
				|| File.Exists(InitDetails.NuspecPath)
				|| File.Exists(InitDetails.ToolsSimpleSyndicateNugetUtilPath))
			{
				Program.OutputError("Project has already been initialised; use reinit if required");
				return false;
			}
			return true;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Git", Justification = "Git is spelt correctly.")]
		public static void InitGitAttributes()
		{
			if (!File.Exists(InitDetails.GitattributesPath))
			{
				ExecuteOperation.Execute(() => { File.WriteAllText(InitDetails.GitattributesPath, Properties.Resources.dotgitattributes); });
				Program.OutputInfo("Added " + InitDetails.GitattributesFileName);
			}
			else
			{
				Program.OutputInfo("Not adding " + InitDetails.GitattributesFileName + " as it already exists");
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Git", Justification = "Git is spelt correctly.")]
		public static void InitGitIgnore()
		{
			if (!File.Exists(InitDetails.GitignorePath))
			{
				ExecuteOperation.Execute(() => { File.WriteAllText(InitDetails.GitignorePath, Properties.Resources.dotgitignore); });
				Program.OutputInfo("Added " + InitDetails.GitignoreFileName);
			}
			else
			{
				Program.OutputInfo("Not adding " + InitDetails.GitignoreFileName + " as it already exists");
			}
		}

		public static void InitVisualStudioProjectFile()
		{
			if (VisualStudioDetails.ProjectXmlDocumentationFile)
			{
				InitDetails.VsProject.AddDebugProperty("DocumentationFile", "bin\\Debug\\" + VisualStudioDetails.ProjectName + ".xml");
				InitDetails.VsProject.AddReleaseProperty("DocumentationFile", "bin\\Release\\" + VisualStudioDetails.ProjectName + ".xml");
				Program.OutputInfo("Enabled XML documentation for Visual Studio project");
			}
			if (VisualStudioDetails.ProjectTreatWarningsAsErrors)
			{
				InitDetails.VsProject.AddDebugProperty("TreatWarningsAsErrors", "true");
				InitDetails.VsProject.AddReleaseProperty("TreatWarningsAsErrors", "true");
				Program.OutputInfo("Enabled treating warnings as errors for Visual Studio project");
			}
			if (VisualStudioDetails.ProjectRunCodeAnalysisOnRelease)
			{
				InitDetails.VsProject.AddDebugProperty("RunCodeAnalysis", "false");
				InitDetails.VsProject.AddReleaseProperty("RunCodeAnalysis", "true");
				Program.OutputInfo("Enabled code analysis for Visual Studio project");
			}
			if (VisualStudioDetails.ProjectCodeAnalysisAllRulesRuleSet)
			{
				InitDetails.VsProject.AddDebugProperty("CodeAnalysisRuleSet", "AllRules.ruleset");
				InitDetails.VsProject.AddReleaseProperty("CodeAnalysisRuleSet", "AllRules.ruleset");
				Program.OutputInfo("Enabled Microsoft All Rules for code analysis for Visual Studio project");
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase", Justification="String has to be in lowercase.")]
		public static void InitReadme()
		{
			ExecuteOperation.Execute(() => { File.WriteAllText(InitDetails.ReadmePath, Properties.Resources.NuGetStandardPackage_README); });
			Replace.ReplaceCommand.ReplaceString(InitDetails.ReadmePath, "$documentationLink$", InitDetails.DocumentationLink);
			Replace.ReplaceCommand.ReplaceString(InitDetails.ReadmePath, "$trackerLink$", InitDetails.TrackerLink);
			Replace.ReplaceCommand.ReplaceString(InitDetails.ReadmePath, "$project$", InitDetails.VsProject.ProjectName);
			Replace.ReplaceCommand.ReplaceString(InitDetails.ReadmePath, "$project.lowercase$", InitDetails.VsProject.ProjectName.ToLowerInvariant());
			Replace.ReplaceCommand.ReplaceString(InitDetails.ReadmePath, "$summary$", InitDetails.NuspecSummary);
			Program.OutputInfo("Created " + InitDetails.ReadmeFileName);
		}

		public static void AddReadmeToVisualStudioSolution()
		{
			InitDetails.VsProject.SolutionFile.AddItemToSolutionItems(InitDetails.ReadmeFileName);
			Program.OutputInfo("Added " + InitDetails.ReadmeFileName + " to Visual Studio solution");
		}

		public static void InitTools()
		{
			if (!Directory.Exists(InitDetails.ToolsPath))
			{
				ExecuteOperation.Execute(() => { Directory.CreateDirectory(InitDetails.ToolsPath); });
			}
			ExecuteOperation.Execute(() => { File.WriteAllBytes(InitDetails.ToolsSimpleSyndicateNugetUtilPath, Properties.Resources.tools_SimpleSyndicate_NuGetUtil); });
			Program.OutputInfo("Created " + InitDetails.ToolsSimpleSyndicateNugetUtilFileName);
		}

		public static void InitNuspec()
		{
			InitNuspec(Properties.Resources.NuGetStandardPackage_Package);
		}

		public static void InitNuspec(string contents)
		{
			InitNuspec(InitDetails.NuspecPath, contents);
		}

		public static void InitNuspec(string path, string contents)
		{
			ExecuteOperation.Execute(() => { File.WriteAllText(path, contents); });
			var nuspecFile = NuspecFileFactory.Open(path, true);
			nuspecFile.Version = InitDetails.NuspecVersion;
			nuspecFile.Authors = InitDetails.NuspecAuthors;
			nuspecFile.Owners = InitDetails.NuspecOwners;
			nuspecFile.ProjectUrl = InitDetails.NuspecProjectUrl;
			nuspecFile.Summary = InitDetails.NuspecSummary;
			nuspecFile.Description = InitDetails.NuspecDescription;
			nuspecFile.ReleaseNotes = InitDetails.NuspecReleaseNotes;
			nuspecFile.RequireLicenseAcceptance = InitDetails.NuspecRequireLicenseAcceptance;
			nuspecFile.Copyright = InitDetails.NuspecCopyright;
			nuspecFile.Tags = InitDetails.NuspecTags;
			Replace.ReplaceCommand.ReplaceString(path, "$project$", InitDetails.VsProject.ProjectName);
			Program.OutputInfo("Created " + nuspecFile.FileName);
		}

		public static void AddNuspecToVisualStudioProject()
		{
			InitDetails.VsProject.AddItemBuildTypeNone(InitDetails.VsProject.ProjectName + ".nuspec", "Designer");
			Program.OutputInfo("Added " + InitDetails.VsProject.ProjectName + ".nuspec to Visual Studio project");
		}

		public static void InitNuGetPush(string[] args)
		{
			InitNuGetPush(args, Properties.Resources.NuGetStandardPackage_nuget_push_push_UpdateDependencies, Properties.Resources.NuGetStandardPackage_nuget_push_push_BuildAndPush);
		}

		public static void InitNuGetPush(string[] args, string pushUpdateDependencies, string pushBuildAndPush)
		{
			string nugetPush = Properties.Resources.NuGetStandardPackage_nuget_push;

			nugetPush = nugetPush.Replace("$:PreCheck$", !String.IsNullOrWhiteSpace(CommandUtility.GetArgument(args, "ss")) ? Properties.Resources.NuGetStandardPackage_nuget_push_SimpleSyndicate_precheck : String.Empty);

			nugetPush = nugetPush.Replace("$:checkToolsGit$", String.IsNullOrWhiteSpace(CommandUtility.GetArgument(args, "nogit")) ? Properties.Resources.NuGetStandardPackage_nuget_push_SimpleSyndicate_checkTools_git : String.Empty);

			nugetPush = nugetPush.Replace("$:pushUpdateDependencies$", pushUpdateDependencies);
			nugetPush = nugetPush.Replace("$:pushBuildAndPush$", pushBuildAndPush);

			nugetPush = nugetPush.Replace("$:pushNuGetServer$", String.IsNullOrWhiteSpace(CommandUtility.GetArgument(args, "nugetServer")) ? String.Empty : " -s " + CommandUtility.GetArgument(args, "nugetServer"));

			nugetPush = nugetPush.Replace("$:pushGitTags$", String.IsNullOrWhiteSpace(CommandUtility.GetArgument(args, "nogit")) ? Properties.Resources.NuGetStandardPackage_nuget_push_SimpleSyndicate_push_GitTags : String.Empty);
			nugetPush = nugetPush.Replace("$:pushGitCommit$", String.IsNullOrWhiteSpace(CommandUtility.GetArgument(args, "nogit")) ? Properties.Resources.NuGetStandardPackage_nuget_push_SimpleSyndicate_push_GitCommit : String.Empty);

			nugetPush = nugetPush.Replace("$:pushBuildHelp$", !String.IsNullOrWhiteSpace(CommandUtility.GetArgument(args, "ss")) ? Properties.Resources.NuGetStandardPackage_nuget_push_SimpleSyndicate_push_BuildHelp : String.Empty);
			nugetPush = nugetPush.Replace("$:pushGitHelpTags$", !String.IsNullOrWhiteSpace(CommandUtility.GetArgument(args, "ss")) ? Properties.Resources.NuGetStandardPackage_nuget_push_SimpleSyndicate_push_GitHelpTags : String.Empty);
			nugetPush = nugetPush.Replace("$:pushGitCommitHelpSolution$", !String.IsNullOrWhiteSpace(CommandUtility.GetArgument(args, "ss")) ? Properties.Resources.NuGetStandardPackage_nuget_push_SimpleSyndicate_push_GitCommitHelpSolution : String.Empty);
			nugetPush = nugetPush.Replace("$:pushGitCommitHelp$", !String.IsNullOrWhiteSpace(CommandUtility.GetArgument(args, "ss")) ? Properties.Resources.NuGetStandardPackage_nuget_push_SimpleSyndicate_push_GitCommitHelp : String.Empty);

			// remove any multiple blank lines just to make it nicer to read
			while (nugetPush.IndexOf("\r\n\r\n\r\n", StringComparison.CurrentCultureIgnoreCase) != -1)
			{
				nugetPush = nugetPush.Replace("\r\n\r\n\r\n", "\r\n\r\n");
			}
			ExecuteOperation.Execute(() => { File.WriteAllText(InitDetails.NugetPushPath, nugetPush); });

			// perform any replacements
			Replace.ReplaceCommand.ReplaceString(InitDetails.NugetPushPath, "$project$", InitDetails.VsProject.ProjectName);
			Replace.ReplaceCommand.ReplaceString(InitDetails.NugetPushPath, "$tagprefix$", String.IsNullOrWhiteSpace(CommandUtility.GetArgument(args, "gitPrefixTagWithProjectName")) ? String.Empty : InitDetails.VsProject.ProjectName + "_");
			Replace.ReplaceCommand.ReplaceString(InitDetails.NugetPushPath, "$messageprefix$", String.IsNullOrWhiteSpace(CommandUtility.GetArgument(args, "gitPrefixTagWithProjectName")) ? String.Empty : InitDetails.VsProject.ProjectName + " ");

			Program.OutputInfo("Created " + InitDetails.NugetPushFileName);
		}

		public static void AddNuGetPushToVisualStudioProject()
		{
			InitDetails.VsProject.AddItemBuildTypeNone(InitDetails.NugetPushFileName);
			Program.OutputInfo("Added " + InitDetails.NugetPushFileName + " to Visual Studio project");
		}

		public static void InitCodeAnalysisDictionary()
		{
			if (InitDetails.CodeAnalysisDictionary)
			{
				ExecuteOperation.Execute(() => { File.WriteAllText(Path.Combine(InitDetails.VsProject.Path, "CodeAnalysisDictionary.xml"), Properties.Resources.VS2013Template_Project_CodeAnalysisDictionary); });
				Program.OutputInfo("Created CodeAnalysisDictionary.xml");
			}
		}

		public static void AddCodeAnalysisDictionaryToVisualStudioProject()
		{
			if (InitDetails.CodeAnalysisDictionary)
			{
				InitDetails.VsProject.AddItemBuildTypeCodeAnalysisDictionary("CodeAnalysisDictionary.xml");
				Program.OutputInfo("Added CodeAnalysisDictionary.xml to Visual Studio project");
			}
		}

		public static void InitGlobalSuppressions()
		{
			ExecuteOperation.Execute(() => { File.WriteAllText(Path.Combine(InitDetails.VsProject.Path, "GlobalSuppressions.cs"), Properties.Resources.VS2013Template_Project_GlobalSuppressions); });
			Program.OutputInfo("Created GlobalSuppressions.cs");
		}

		public static void AddGlobalSuppressionsToVisualStudioProject()
		{
			InitDetails.VsProject.AddItemBuildTypeCompile("GlobalSuppressions.cs");
			Program.OutputInfo("Added GlobalSuppressions.cs to Visual Studio project");
		}

		public static void InitStyleCopSettings()
		{
			if (InitDetails.StyleCop)
			{
				ExecuteOperation.Execute(() => { File.WriteAllText(Path.Combine(InitDetails.VsProject.SolutionFile.Path, "Settings.StyleCop"), Properties.Resources.VS2013Template_Settings); });
				Program.OutputInfo("Created Visual Studio solution Settings.StyleCop");

				ExecuteOperation.Execute(() => { File.WriteAllText(Path.Combine(InitDetails.VsProject.Path, "Settings.StyleCop"), Properties.Resources.VS2013Template_Project_Settings); });
				Program.OutputInfo("Created Visual Studio project Settings.StyleCop");

				ExecuteOperation.Execute(() => { File.WriteAllText(Path.Combine(InitDetails.TestsVsProject.Path, "Settings.StyleCop"), Properties.Resources.VS2013Template_UnitTestProject_Settings); });
				Program.OutputInfo("Created Visual Studio tests project Settings.StyleCop");
			}
		}

		public static void AddStyleCopSettingsToVisualStudioSolution()
		{
			if (InitDetails.StyleCop)
			{
				InitDetails.VsProject.SolutionFile.AddItemToSolutionItems("Settings.StyleCop");
				Program.OutputInfo("Added Settings.StyleCop to Visual Studio solution");

				InitDetails.VsProject.AddItemBuildTypeNone("Settings.StyleCop");
				Program.OutputInfo("Added Settings.StyleCop to Visual Studio project");

				InitDetails.TestsVsProject.AddItemBuildTypeNone("Settings.StyleCop");
				Program.OutputInfo("Added Settings.StyleCop to Visual Studio tests project");

				if (InitDetails.StyleCopBuildIntegration)
				{
					InitDetails.VsProject.AddImportProject("$(ProgramFiles)\\MSBuild\\StyleCop\\v4.7\\StyleCop.targets");
					Program.OutputInfo("Enabled StyleCop build integration for Visual Studio project");

					InitDetails.TestsVsProject.AddImportProject("$(ProgramFiles)\\MSBuild\\StyleCop\\v4.7\\StyleCop.targets");
					Program.OutputInfo("Enabled StyleCop build integration for Visual Studio tests project");
				}

				if (InitDetails.StyleCopTreatErrorsAsBuildErrors)
				{
					InitDetails.VsProject.AddGlobalProperty("StyleCopTreatErrorsAsWarnings", "false");
					Program.OutputInfo("Enabled treating StyleCop errors as build errors for Visual Studio project");

					InitDetails.TestsVsProject.AddGlobalProperty("StyleCopTreatErrorsAsWarnings", "false");
					Program.OutputInfo("Enabled treating StyleCop errors as build errors for Visual Studio tests project");
				}
			}
		}

		public static void RemoveClass1FromVisualStudioProject()
		{
			InitDetails.VsProject.RemoveItemBuildTypeCompile("Class1.cs");
			ExecuteOperation.Execute(() => { File.Delete(Path.Combine(InitDetails.VsProject.Path, "Class1.cs")); });
			Program.OutputInfo("Removed Class1.cs from Visual Studio project");
		}

		public static void RemoveUnitTest1FromVisualStudioTestsProject()
		{
			InitDetails.TestsVsProject.RemoveItemBuildTypeCompile("UnitTest1.cs");
			ExecuteOperation.Execute(() => { File.Delete(Path.Combine(InitDetails.TestsVsProject.Path, "UnitTest1.cs")); });
			Program.OutputInfo("Removed UnitTest1.cs from Visual Studio tests project");
		}

		public static void InitAssemblyInfo()
		{
			var directory = new DirectoryInfo(InitDetails.VsProject.SolutionFile.Path);
			var files = directory.GetFiles("AssemblyInfo.cs", SearchOption.AllDirectories);
			foreach (var file in files)
			{
				var assemblyInfo = new VersionUpdate.AssemblyInfo(file.FullName);
				assemblyInfo.Company = InitDetails.AssemblyInfoCompany;
				assemblyInfo.Configuration = InitDetails.AssemblyInfoConfiguration;
				assemblyInfo.Copyright = InitDetails.AssemblyInfoCopyright;
				assemblyInfo.Culture = InitDetails.AssemblyInfoCulture;
				assemblyInfo.Description = InitDetails.AssemblyInfoDescription;
				assemblyInfo.Trademark = InitDetails.AssemblyInfoTrademark;

				assemblyInfo.CLSCompliant = InitDetails.AssemblyInfoCLSCompliant;
				Program.OutputInfo("Updated " + file.FullName.Replace(InitDetails.VsProject.SolutionFile.Path + System.IO.Path.DirectorySeparatorChar, String.Empty));
			}
		}
	}
}
