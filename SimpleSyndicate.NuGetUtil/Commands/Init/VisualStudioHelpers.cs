﻿using System;
using System.IO;

namespace SimpleSyndicate.NuGetUtil.Commands.Init
{
	public static class VisualStudioHelpers
	{
		public static string DetermineProjectPath(string[] args)
		{
			var path = CommandUtility.GetArgument(args, "path");
			return FindProject(args, String.IsNullOrWhiteSpace(path) ? System.Environment.CurrentDirectory : path, CommandUtility.GetArgument(args, "name"));
		}

		public static string DetermineTestsProjectPath(string[] args)
		{
			var path = CommandUtility.GetArgument(args, "path");
			return FindTestsProject(String.IsNullOrWhiteSpace(path) ? System.Environment.CurrentDirectory : path);
		}

		private static string FindProject(string[] args, string path, string name)
		{
			if (!Directory.Exists(path))
			{
				ExecuteOperation.Execute(() => { Directory.CreateDirectory(path); });
			}
			// if there's a project found, use it
			if (PathHasFileWithEndingInIt(path, ".csproj"))
			{
				return path;
			}
			// no project, but if there's a solution found, look in sub-directories for a project
			if (PathHasFileWithEndingInIt(path, ".sln"))
			{
				foreach (var directory in Directory.EnumerateDirectories(path))
				{
					if (PathHasFileWithEndingInIt(directory, ".csproj"))
					{
						return directory;
					}
				}
			}
			// nothing found, so create template solution
			Program.OutputInfo("No Visual Studio project found, creating template project.");
			if (String.IsNullOrWhiteSpace(name))
			{
				Program.OutputError("No name specified for template project.");
				return null;
			}
			return CreateProject(args, path, name);
		}

		private static string FindTestsProject(string path)
		{
			// if there's a project found, use it
			if (PathHasFileWithEndingInIt(path, ".Tests.csproj"))
			{
				return path;
			}
			// no project, but if there's a solution found, look in sub-directories for a project
			if (PathHasFileWithEndingInIt(path, ".sln"))
			{
				foreach (var directory in Directory.EnumerateDirectories(path))
				{
					if (PathHasFileWithEndingInIt(directory, ".Tests.csproj"))
					{
						return directory;
					}
				}
			}
			return null;
		}

		private static bool PathHasFileWithEndingInIt(string path, string endsWith)
		{
			foreach (var file in Directory.EnumerateFiles(path))
			{
				if (file.EndsWith(endsWith, StringComparison.OrdinalIgnoreCase))
				{
					return true;
				}
			}
			return false;
		}

		private static string CreateProject(string[] args, string path, string name)
		{
			VisualStudioDetails.DetermineDetails(args, path, name);
			CreateSolution();
			CreateProject();
			CreateProjectClass1();
			CreateProjectPropertiesAssemblyInfo();
			CreateTestsProject();
			CreateTestsProjectUnitTest1();
			CreateTestsProjectPropertiesAssemblyInfo();
			return VisualStudioDetails.ProjectRoot;
		}

		private static void CreateSolution()
		{
			var solutionFile = Properties.Resources.VS2013Template;
			solutionFile = solutionFile.Replace("$ProjectName$", VisualStudioDetails.ProjectName);
			solutionFile = solutionFile.Replace("$ProjectGuid$", VisualStudioDetails.ProjectGuid);
			solutionFile = solutionFile.Replace("$UnitTestProjectName$", VisualStudioDetails.TestsProjectName);
			solutionFile = solutionFile.Replace("$UnitTestProjectGuid$", VisualStudioDetails.TestsProjectGuid);
			ExecuteOperation.Execute(() => { File.WriteAllText(VisualStudioDetails.SolutionPath, solutionFile); });
		}

		private static void CreateProject()
		{
			if (!Directory.Exists(VisualStudioDetails.ProjectRoot))
			{
				ExecuteOperation.Execute(() => { Directory.CreateDirectory(VisualStudioDetails.ProjectRoot); });
			}
			var projectFile = Properties.Resources.VS2013Template_Project;
			if (VisualStudioDetails.ProjectIsWebProject)
			{
				projectFile = Properties.Resources.VS2013Template_WebProject;
			}
			projectFile = projectFile.Replace("$ProjectName$", VisualStudioDetails.ProjectName);
			projectFile = projectFile.Replace("$ProjectGuid$", VisualStudioDetails.ProjectGuid);
			ExecuteOperation.Execute(() => { File.WriteAllText(VisualStudioDetails.ProjectPath, projectFile); });
			var vsProject = new VisualStudioProjectFile(VisualStudioDetails.ProjectPath);
		}

		private static void CreateProjectClass1()
		{
			var class1 = Properties.Resources.VS2013Template_Project_Class1;
			class1 = class1.Replace("$ProjectName$", VisualStudioDetails.ProjectName);
			ExecuteOperation.Execute(() => { File.WriteAllText(VisualStudioDetails.ProjectClass1Path, class1); });
		}

		private static void CreateProjectPropertiesAssemblyInfo()
		{
			if (!Directory.Exists(VisualStudioDetails.ProjectPropertiesPath))
			{
				ExecuteOperation.Execute(() => { Directory.CreateDirectory(VisualStudioDetails.ProjectPropertiesPath); });
			}
			var assemblyInfo = Properties.Resources.VS2013Template_Project_Properties_AssemblyInfo;
			assemblyInfo = assemblyInfo.Replace("$ProjectName$", VisualStudioDetails.ProjectName);
			assemblyInfo = assemblyInfo.Replace("$ProjectAssemblyGuid$", VisualStudioDetails.ProjectAssemblyGuid);
			ExecuteOperation.Execute(() => { File.WriteAllText(VisualStudioDetails.ProjectPropertiesAssemblyInfoPath, assemblyInfo); });
		}

		private static void CreateTestsProject()
		{
			if (!Directory.Exists(VisualStudioDetails.TestsProjectRoot))
			{
				ExecuteOperation.Execute(() => { Directory.CreateDirectory(VisualStudioDetails.TestsProjectRoot); });
			}
			var testsProjectFile = Properties.Resources.VS2013Template_UnitTestProject;
			testsProjectFile = testsProjectFile.Replace("$UnitTestProjectName$", VisualStudioDetails.TestsProjectName);
			testsProjectFile = testsProjectFile.Replace("$UnitTestProjectGuid$", VisualStudioDetails.TestsProjectGuid);
			ExecuteOperation.Execute(() => { File.WriteAllText(VisualStudioDetails.TestsProjectPath, testsProjectFile); });
		}

		private static void CreateTestsProjectUnitTest1()
		{
			var unitTest1 = Properties.Resources.VS2013Template_UnitTestProject_UnitTest1;
			unitTest1 = unitTest1.Replace("$UnitTestProjectName$", VisualStudioDetails.ProjectName);
			ExecuteOperation.Execute(() => { File.WriteAllText(VisualStudioDetails.TestsProjectUnitTest1Path, unitTest1); });
		}

		private static void CreateTestsProjectPropertiesAssemblyInfo()
		{
			if (!Directory.Exists(VisualStudioDetails.TestsProjectPropertiesPath))
			{
				ExecuteOperation.Execute(() => { Directory.CreateDirectory(VisualStudioDetails.TestsProjectPropertiesPath); });
			}
			var assemblyInfo = Properties.Resources.VS2013Template_UnitTestProject_Properties_AssemblyInfo;
			assemblyInfo = assemblyInfo.Replace("$UnitTestProjectName$", VisualStudioDetails.TestsProjectName);
			assemblyInfo = assemblyInfo.Replace("$UnitTestProjectAssemblyGuid$", VisualStudioDetails.TestsProjectAssemblyGuid);
			ExecuteOperation.Execute(() => { File.WriteAllText(VisualStudioDetails.TestsProjectPropertiesAssemblyInfoPath, assemblyInfo); });
		}
	}
}
