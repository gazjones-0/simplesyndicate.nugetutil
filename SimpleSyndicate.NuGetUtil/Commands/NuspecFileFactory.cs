﻿using System;
using System.Globalization;

namespace SimpleSyndicate.NuGetUtil.Commands
{
	public static class NuspecFileFactory
	{
		private static NuspecFile Create(string path)
		{
			if (System.IO.File.Exists(path))
			{
				return new NuspecFile(path);
			}
			foreach (var file in System.IO.Directory.EnumerateFiles(path))
			{
				if (file.EndsWith(".nuspec", StringComparison.OrdinalIgnoreCase))
				{
					return new NuspecFile(file);
				}
			}
			throw new InvalidOperationException();
		}

		public static NuspecFile Open(VisualStudioProjectFile projectFile)
		{
			if (projectFile == null)
			{
				throw new ArgumentNullException("projectFile");
			}
			return new NuspecFile(projectFile.FileName.Replace(".csproj", ".nuspec"));
		}

		public static NuspecFile Open(string path, bool outputInfo)
		{
			try
			{
				var nuspecFile = NuspecFileFactory.Create(path);
				if (outputInfo)
				{
					Program.OutputInfo(String.Format(CultureInfo.CurrentCulture, "Nuspec file is {0}", nuspecFile.FileName));
				}
				return nuspecFile;
			}
			catch (System.InvalidOperationException)
			{
				Program.OutputError("No .nuspec file found");
				throw;
			}
			catch (System.IO.DirectoryNotFoundException)
			{
				Program.OutputError("Invalid path specified");
				throw;
			}
		}
	}
}
