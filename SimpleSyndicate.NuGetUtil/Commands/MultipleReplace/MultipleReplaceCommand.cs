﻿using System;
using System.IO;
using System.Linq;

namespace SimpleSyndicate.NuGetUtil.Commands.MultipleReplace
{
	public static class MultipleReplaceCommand
	{
		public static void Execute(string[] args)
		{
			if (args == null)
			{
				throw new ArgumentNullException("args");
			}
			if (CommandUtility.IsCommand(args, "MultiReplace"))
			{
				Program.CommandExecuted();
				if (args.Count() < 2)
				{
					Program.OutputError("No path specified to perform multiple string replacement on");
					return;
				}
				if (args.Count() < 3)
				{
					Program.OutputError("No old value to find specified");
					return;
				}
				if (args.Count() < 4)
				{
					Program.OutputError("No new value to replace specified");
					return;
				}
				string searchPattern = String.Empty;
				string newFileExtension = String.Empty;
				if (args.Count() > 4)
				{
					searchPattern = args[4];
					if (args.Count() > 5)
					{
						newFileExtension = args[5];
					}
				}
				ReplaceString(args[1], args[2], args[3], searchPattern, newFileExtension);
			}
		}

		public static void ReplaceString(string path, string oldValue, string newValue, string searchPattern, string newFileExtension)
		{
			var directory = new DirectoryInfo(path);
			foreach (var subDirectory in directory.GetDirectories())
			{
				ReplaceString(subDirectory.FullName, oldValue, newValue, searchPattern, newFileExtension);
			}

			FileInfo[] files;
			if (String.IsNullOrEmpty(searchPattern))
			{
				files = directory.GetFiles();
			}
			else
			{
				files = directory.GetFiles(searchPattern);
			}
			foreach (var file in files)
			{
				var contents = File.ReadAllText(file.FullName);
				if (contents.IndexOf(oldValue, StringComparison.Ordinal) >= 0)
				{
					contents = contents.Replace(oldValue, newValue);
					ExecuteOperation.Execute(() => { File.WriteAllText(file.FullName, contents); });
					if (!String.IsNullOrWhiteSpace(newFileExtension))
					{
						var newFileName = file.Name;
						newFileName = newFileName.Substring(0, newFileName.Length - file.Extension.Length);
						if (!newFileExtension.StartsWith(".", StringComparison.Ordinal))
						{
							newFileName = newFileName + ".";
						}
						newFileName = newFileName + newFileExtension;

						var newFileFullName = file.FullName;
						newFileFullName = newFileFullName.Substring(0, newFileFullName.Length - file.Name.Length);
						newFileFullName = newFileFullName + newFileName;
						ExecuteOperation.Execute(() => { file.MoveTo(newFileFullName); });
					}
				}
			}
		}
	}
}
