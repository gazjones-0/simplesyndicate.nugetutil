﻿namespace SimpleSyndicate.NuGetUtil.Commands
{
	public enum VersionUpdateComponent
	{
		Unknown = -1,
		Major = 0,
		Minor = 1,
		Point = 2,
		Prerelease = 3
	}
}
