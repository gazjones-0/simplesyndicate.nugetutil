﻿using System;
using System.Globalization;
using System.Linq;

namespace SimpleSyndicate.NuGetUtil.Commands.PrependLine
{
	public static class PrependLineCommand
	{
		public static void Execute(string[] args)
		{
			if (args == null)
			{
				throw new ArgumentNullException("args");
			}
			if (CommandUtility.IsCommand(args, "PrependLine"))
			{
				Program.CommandExecuted();
				if (args.Count() < 2)
				{
					Program.OutputError("No file specified to prepend");
					return;
				}
				if (args.Count() < 3)
				{
					Program.OutputError("No line specified to prepend");
					return;
				}
				Prepend(args[1], args[2]);
			}
		}

		public static void Prepend(string path, string line)
		{
			if (!System.IO.File.Exists(path))
			{
				Program.OutputError("File to prepend does not exist");
			}
			var contents = System.IO.File.ReadAllText(path);
			Program.OutputInfo(String.Format(CultureInfo.CurrentCulture, "File is {0}", path));
			if (!contents.StartsWith(line + "\r\n", StringComparison.OrdinalIgnoreCase))
			{
				contents = line + "\r\n" + contents;
				Program.OutputInfo(String.Format(CultureInfo.CurrentCulture, "Line is {0}", line));
				ExecuteOperation.Execute(() => { System.IO.File.WriteAllText(path, contents); });
				Program.OutputInfo("Line prepended");
			}
			else
			{
				Program.OutputInfo("File already starts with line, left as-is");
			}
		}
	}
}
