﻿using System.Collections.Generic;

namespace SimpleSyndicate.NuGetUtil.Commands
{
	public class NuGetPackage
	{
		public NuGetPackage()
		{
			Dependencies = new List<NuGetPackage>();
		}

		public string Id { get; set; }

		public string Version { get; set; }

		public string Path { get; set; }

		public IList<NuGetPackage> Dependencies { get; private set; }
	}
}
