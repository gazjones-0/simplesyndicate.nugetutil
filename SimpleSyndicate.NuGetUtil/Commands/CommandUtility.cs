﻿using System;
using System.Linq;

namespace SimpleSyndicate.NuGetUtil.Commands
{
	public static class CommandUtility
	{
		public static bool IsCommand(string[] args, string command)
		{
			if (String.Compare(CommandUtility.GetCommand(args), command, StringComparison.OrdinalIgnoreCase) == 0)
			{
				return true;
			}
			return false;
		}

		public static bool AreArgumentsValid(string[] args)
		{
			try
			{
				GetArgument(args, "doesntMatter");
				return true;
			}
			catch(InvalidArgumentsException)
			{
				return false;
			}
		}

		public static string GetCommand(string[] args)
		{
			if (args == null)
			{
				throw new ArgumentNullException("args");
			}
			if (args.Count() == 0)
			{
				return String.Empty;
			}
			return GetArgumentName(args[0]);
		}

		public static string GetArgument(string[] args, string argumentName)
		{
			if (args == null)
			{
				throw new ArgumentNullException("args");
			}
			var parsedArgs = new System.Collections.Generic.Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);
			// the first argument is always the command, so ignore that one
			int position = 1;
			while (position < args.Length)
			{
				var name = GetArgumentName(args[position]);
				position++;
				if (position >= args.Length)
				{
					throw new InvalidArgumentsException();
				}
				parsedArgs.Add(name, args[position]);
				position++;
			}
			if (parsedArgs.ContainsKey(argumentName))
			{
				return parsedArgs[argumentName];
			}
			return null;
		}

		public static string GetArgument(string[] args, string argumentName, string defaultIfNotSpecified)
		{
			var argument = GetArgument(args, argumentName);
			if (argument != null)
			{
				return argument;
			}
			return defaultIfNotSpecified;
		}

		private static string GetArgumentName(string argument)
		{
			if (argument.StartsWith("--", StringComparison.OrdinalIgnoreCase))
			{
				argument = argument.Substring(2);
			}
			if (argument.StartsWith("-", StringComparison.OrdinalIgnoreCase))
			{
				argument = argument.Substring(1);
			}
			if (argument.StartsWith("/", StringComparison.OrdinalIgnoreCase))
			{
				argument = argument.Substring(1);
			}
			return argument;
		}
	}
}
