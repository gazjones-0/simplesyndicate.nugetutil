﻿using System;
using System.IO;

namespace SimpleSyndicate.NuGetUtil.Commands.VersionUpdate
{
	public static class VersionHistoryFileFactory
	{
		public static VersionHistoryFile Create()
		{
			return Create(System.Environment.CurrentDirectory);
		}

		public static VersionHistoryFile Create(string path)
		{
			// work out what the common prefix being used by this project is
			var currentDirectory = new DirectoryInfo(path);
			string commonPrefix = currentDirectory.Name;
			if (commonPrefix.IndexOf('.') >= 0)
			{
				commonPrefix = commonPrefix.Substring(0, commonPrefix.IndexOf('.'));
			}

			// see if there's a directory of the form <common prefix>.Help
			var directory = Directory.GetParent(path).Parent;
			foreach (var subDirectory in directory.GetDirectories())
			{
				if (String.CompareOrdinal(subDirectory.Name, commonPrefix + ".Help") == 0)
				{
					var commonPrefixfile = Find(subDirectory, currentDirectory.Name + ".aml");
					if (commonPrefixfile != null)
					{
						return new VersionHistoryFile(commonPrefixfile.FullName);
					}
				}
			}

			var file = Find(Directory.GetParent(path), "VersionHistory.aml");
			if (file != null)
			{
				return new VersionHistoryFile(file.FullName);
			}
			throw new InvalidOperationException();
		}

		private static FileInfo Find(DirectoryInfo directory, string filenameToFind)
		{
			foreach (var subDirectory in directory.GetDirectories())
			{
				var subDirectoryFile = Find(subDirectory, filenameToFind);
				if (subDirectoryFile != null)
				{
					return subDirectoryFile;
				}
			}
			foreach (var file in directory.GetFiles())
			{
				if (String.Compare(file.Name, filenameToFind, StringComparison.OrdinalIgnoreCase) == 0)
				{
					return file;
				}
			}
			return null;
		}
	}
}
