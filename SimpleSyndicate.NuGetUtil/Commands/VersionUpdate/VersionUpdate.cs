﻿using System;
using System.Globalization;
using System.Linq;

namespace SimpleSyndicate.NuGetUtil.Commands.VersionUpdate
{
	public static class VersionUpdateCommand
	{
		public static void Execute(string[] args)
		{
			if (args == null)
			{
				throw new ArgumentNullException("args");
			}
			if (CommandUtility.IsCommand(args, "VersionUpdate") || CommandUtility.IsCommand(args, "VersionUpdateNoReleaseNotes"))
			{
				Program.CommandExecuted();
				bool noReleaseNotes = false;
				if (args[0].EndsWith("noreleasenotes", StringComparison.OrdinalIgnoreCase))
				{
					noReleaseNotes = true;
				}
				if (args.Count() < 2)
				{
					Program.OutputError("No version component specified to update");
					return;
				}
				var versionUpdateComponent = GetVersionUpdateComponent(args[1]);
				if (versionUpdateComponent == VersionUpdateComponent.Unknown)
				{
					Program.OutputError("Invalid version component specified to update");
					return;
				}
				if (args.Count() > 2)
				{
					IncrementVersion(versionUpdateComponent, args[2], noReleaseNotes);
					return;
				}
				IncrementVersion(versionUpdateComponent, noReleaseNotes);
			}
		}

		public static VersionUpdateComponent GetVersionUpdateComponent(string versionComponent)
		{
			switch (versionComponent)
			{
				case "major":
					return VersionUpdateComponent.Major;
				case "minor":
					return VersionUpdateComponent.Minor;
				case "point":
				case "patch":
					return VersionUpdateComponent.Point;
				case "prerelease":
					return VersionUpdateComponent.Prerelease;
			}
			return VersionUpdateComponent.Unknown;
		}

		public static void IncrementVersion(VersionUpdateComponent component, bool noReleaseNotes)
		{
			IncrementVersion(component, System.Environment.CurrentDirectory, noReleaseNotes);
		}

		public static void IncrementVersion(VersionUpdateComponent component, string path, bool noReleaseNotes)
		{
			Program.OutputInfo("Incrementing .nuspec version");
			Program.OutputInfo(String.Format(CultureInfo.CurrentCulture, "Path is {0}", path));
			NuspecFile nuspecFile;
			try
			{
				nuspecFile = NuspecFileFactory.Open(path, true);
			}
			catch (System.InvalidOperationException)
			{
				return;
			}
			catch (System.IO.DirectoryNotFoundException)
			{
				return;
			}
			var updatedVersion = GetUpdatedVersion(nuspecFile, component);
			if (noReleaseNotes == false)
			{
				VersionHistoryFile versionHistoryFile;
				try
				{
					versionHistoryFile = GetVersionHistoryFile(path);
				}
				catch (System.InvalidOperationException)
				{
					return;
				}
				var updatedReleaseNotes = GetUpdatedReleaseNotes(versionHistoryFile);
				nuspecFile.ReleaseNotes = updatedReleaseNotes;
			}
			nuspecFile.Version = GetVersion(updatedVersion);
			Program.OutputInfo("Updated .nuspec file");
			AssemblyInfo assemblyInfoFile;
			try
			{
				assemblyInfoFile = GetAssemblyInfoFile(nuspecFile);
			}
			catch (System.IO.DirectoryNotFoundException)
			{
				return;
			}
			catch (System.IO.FileNotFoundException)
			{
				return;
			}
			assemblyInfoFile.Version = GetVersionWithoutPrerelease(updatedVersion);
			assemblyInfoFile.FileVersion = GetVersionWithoutPrerelease(updatedVersion);
			Program.OutputInfo("Updated Properties\\AssemblyInfo.cs");

			foreach (var folder in System.IO.Directory.EnumerateDirectories(new System.IO.DirectoryInfo(nuspecFile.Path).Parent.FullName))
			{
				if (System.IO.File.Exists(folder + "\\Properties\\AssemblyInfo.cs"))
				{
					if ((nuspecFile.Path + "\\Properties\\AssemblyInfo.cs") != (folder + "\\Properties\\AssemblyInfo.cs"))
					{
						assemblyInfoFile = new AssemblyInfo(folder + "\\Properties\\AssemblyInfo.cs");
						Program.OutputInfo(String.Format(CultureInfo.CurrentCulture, "Path is {0}", folder));
						assemblyInfoFile.Version = GetVersionWithoutPrerelease(updatedVersion);
						assemblyInfoFile.FileVersion = GetVersionWithoutPrerelease(updatedVersion);
						Program.OutputInfo("Updated Properties\\AssemblyInfo.cs");
					}
				}
			}
		}

		private static VersionHistoryFile GetVersionHistoryFile(string path)
		{
			try
			{
				var versionHistoryFile = VersionHistoryFileFactory.Create(path);
				Program.OutputInfo(String.Format(CultureInfo.CurrentCulture, "Version history file is {0}", versionHistoryFile.FileName));
				return versionHistoryFile;
			}
			catch (System.InvalidOperationException)
			{
				Program.OutputError("No version history file found");
				throw;
			}
		}

		private static AssemblyInfo GetAssemblyInfoFile(NuspecFile nuspecFile)
		{
			try
			{
				var assemblyInfoFile = new AssemblyInfo(nuspecFile.Path + "\\Properties\\AssemblyInfo.cs");
				return assemblyInfoFile;
			}
			catch (System.IO.DirectoryNotFoundException)
			{
				Program.OutputError("No Properties\\AssemblyInfo.cs file found");
				throw;
			}
			catch (System.IO.FileNotFoundException)
			{
				Program.OutputError("No Properties\\AssemblyInfo.cs file found");
				throw;
			}
		}

		private static string GetUpdatedReleaseNotes(VersionHistoryFile versionHistoryFile)
		{
			var updatedReleaseNotes = versionHistoryFile.LatestVersionHistoryAsText();
			Program.OutputInfo(String.Format(CultureInfo.CurrentCulture, "Release notes are {0}", updatedReleaseNotes));
			return updatedReleaseNotes;
		}

		private static System.Version GetUpdatedVersion(NuspecFile nuspecFile, VersionUpdateComponent component)
		{
			System.Version updatedVersion;
			switch (component)
			{
				case VersionUpdateComponent.Major:
					updatedVersion = VersionFactory.IncrementMajorVersion(nuspecFile);
					break;
				case VersionUpdateComponent.Minor:
					updatedVersion = VersionFactory.IncrementMinorVersion(nuspecFile);
					break;
				case VersionUpdateComponent.Point:
					updatedVersion = VersionFactory.IncrementPointVersion(nuspecFile);
					break;
				case VersionUpdateComponent.Prerelease:
					updatedVersion = VersionFactory.IncrementPrereleaseVersion(nuspecFile);
					break;
				default:
					return null;
			}
			Program.OutputInfo(String.Format(CultureInfo.CurrentCulture, "Updated version is {0}", GetVersion(updatedVersion)));
			return updatedVersion;
		}

		private static string GetVersion(System.Version version)
		{
			if (version.Revision != -1)
			{
				return version.Major + "." + version.Minor + "." + version.Build + "-prerelease" + String.Format(CultureInfo.CurrentCulture, "{0:D3}", version.Revision);
			}
			return version.Major + "." + version.Minor + "." + version.Build;
		}

		private static string GetVersionWithoutPrerelease(System.Version version)
		{
			return version.Major + "." + version.Minor + "." + version.Build;
		}
	}
}
