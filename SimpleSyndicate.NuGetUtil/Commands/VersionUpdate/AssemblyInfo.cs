﻿using System;
using System.IO;

namespace SimpleSyndicate.NuGetUtil.Commands.VersionUpdate
{
	public class AssemblyInfo
	{
		public string Path { get; set; }

		public string FileName { get; set; }

		private string assemblyInfoText;

		public AssemblyInfo(string file)
		{
			FileName = System.IO.Path.GetFileName(file);
			Path = System.IO.Path.GetDirectoryName(file);
			assemblyInfoText = File.ReadAllText(Path + System.IO.Path.DirectorySeparatorChar + FileName);
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "CLS", Justification = "Casing is same as .Net framework.")]
		public bool CLSCompliant
		{
			get
			{
				bool result;
				if (Boolean.TryParse(AttributeValue("AssemblyVersion", false, String.Empty), out result))
				{
					return result;
				}
				return false;
			}
			set
			{
				if (value == true)
				{
					AttributeValue("CLSCompliant", true, "true");
				}
				else
				{
					AttributeValue("CLSCompliant", true, "false");
				}
				ExecuteOperation.Execute(() => { File.WriteAllText(Path + System.IO.Path.DirectorySeparatorChar + FileName, assemblyInfoText); });
			}
		}

		public string Company
		{
			get
			{
				return StringAttributeValue("AssemblyCompany");
			}
			set
			{
				AttributeValue("AssemblyCompany", true, "\"" + value + "\"");
				ExecuteOperation.Execute(() => { File.WriteAllText(Path + System.IO.Path.DirectorySeparatorChar + FileName, assemblyInfoText); });
			}
		}

		public string Configuration
		{
			get
			{
				return StringAttributeValue("AssemblyConfiguration");
			}
			set
			{
				AttributeValue("AssemblyConfiguration", true, "\"" + value + "\"");
				ExecuteOperation.Execute(() => { File.WriteAllText(Path + System.IO.Path.DirectorySeparatorChar + FileName, assemblyInfoText); });
			}
		}

		public string Copyright
		{
			get
			{
				return StringAttributeValue("AssemblyCopyright");
			}
			set
			{
				AttributeValue("AssemblyCopyright", true, "\"" + value + "\"");
				ExecuteOperation.Execute(() => { File.WriteAllText(Path + System.IO.Path.DirectorySeparatorChar + FileName, assemblyInfoText); });
			}
		}

		public string Culture
		{
			get
			{
				return StringAttributeValue("AssemblyCulture");
			}
			set
			{
				AttributeValue("AssemblyCulture", true, "\"" + value + "\"");
				ExecuteOperation.Execute(() => { File.WriteAllText(Path + System.IO.Path.DirectorySeparatorChar + FileName, assemblyInfoText); });
			}
		}

		public string Description
		{
			get
			{
				return StringAttributeValue("AssemblyDescription");
			}
			set
			{
				AttributeValue("AssemblyDescription", true, "\"" + value + "\"");
				ExecuteOperation.Execute(() => { File.WriteAllText(Path + System.IO.Path.DirectorySeparatorChar + FileName, assemblyInfoText); });
			}
		}

		public string FileVersion
		{
			get
			{
				return StringAttributeValue("AssemblyFileVersion");
			}
			set
			{
				AttributeValue("AssemblyFileVersion", true, "\"" + value + "\"");
				ExecuteOperation.Execute(() => { File.WriteAllText(Path + System.IO.Path.DirectorySeparatorChar + FileName, assemblyInfoText); });
			}
		}

		public string Trademark
		{
			get
			{
				return StringAttributeValue("AssemblyTrademark");
			}
			set
			{
				AttributeValue("AssemblyTrademark", true, "\"" + value + "\"");
				ExecuteOperation.Execute(() => { File.WriteAllText(Path + System.IO.Path.DirectorySeparatorChar + FileName, assemblyInfoText); });
			}
		}

		public string Version
		{
			get
			{
				return StringAttributeValue("AssemblyVersion");
			}
			set
			{
				AttributeValue("AssemblyVersion", true, "\"" + value + "\"");
				ExecuteOperation.Execute(() => { File.WriteAllText(Path + System.IO.Path.DirectorySeparatorChar + FileName, assemblyInfoText); });
			}
		}

		private string StringAttributeValue(string name)
		{
			var value = AttributeValue(name, false, String.Empty);
			if (String.IsNullOrWhiteSpace(value))
			{
				return value;
			}
			return value.Substring(1, value.Length - 2);
		}

		private string AttributeValue(string name, bool replaceValue, string newValue)
		{
			var startIndex = 0;
			var firstSectionEnd = assemblyInfoText.Length;
			while(startIndex >= 0)
			{
				int assemblyIndex = assemblyInfoText.IndexOf("[assembly", startIndex, StringComparison.OrdinalIgnoreCase);
				if (assemblyIndex >= 0)
				{
					bool keepLooking = true;
					bool commentedOut = false;
					int offset = 1;
					while (keepLooking && assemblyInfoText.Substring(assemblyIndex - offset, 1) != "\r" && assemblyInfoText.Substring(assemblyIndex - offset, 1) != "\n")
					{
						if (assemblyInfoText.Substring(assemblyIndex - offset, 2) == "//")
						{
							commentedOut = true;
							keepLooking = false;
							startIndex = assemblyIndex + 1;
						}
						offset++;
						if ((assemblyIndex - offset) == 0)
						{
							keepLooking = false;
						}
					}
					if (commentedOut == false)
					{
						var colonIndex = assemblyInfoText.IndexOf(":", assemblyIndex, StringComparison.OrdinalIgnoreCase);
						if (colonIndex >= 0)
						{
							var endIndex = assemblyInfoText.IndexOf("]", colonIndex, StringComparison.OrdinalIgnoreCase);
							if (endIndex >= 0)
							{
								var assembly = assemblyInfoText.Substring(colonIndex + 1, endIndex - colonIndex - 1).Trim();
								if (assembly.StartsWith(name + "(", StringComparison.OrdinalIgnoreCase) && assembly.EndsWith(")", StringComparison.OrdinalIgnoreCase))
								{
									var valueStart = assemblyInfoText.IndexOf("(", colonIndex + 1, StringComparison.OrdinalIgnoreCase) + 1;
									var valueEnd = assemblyInfoText.IndexOf(")", valueStart, StringComparison.OrdinalIgnoreCase);
									if (replaceValue == true)
									{
										assemblyInfoText =
											assemblyInfoText.Substring(0, valueStart)
											+ newValue
											+ assemblyInfoText.Substring(valueEnd);
										return newValue;
									}
									return assemblyInfoText.Substring(valueStart, valueEnd - valueStart);
								}
								else
								{
									if (
										(assembly.StartsWith("AssemblyTitle(", StringComparison.OrdinalIgnoreCase))
										|| (assembly.StartsWith("AssemblyDescription(", StringComparison.OrdinalIgnoreCase))
										|| (assembly.StartsWith("AssemblyConfiguration(", StringComparison.OrdinalIgnoreCase))
										|| (assembly.StartsWith("AssemblyCompany(", StringComparison.OrdinalIgnoreCase))
										|| (assembly.StartsWith("AssemblyProduct(", StringComparison.OrdinalIgnoreCase))
										|| (assembly.StartsWith("AssemblyCopyright(", StringComparison.OrdinalIgnoreCase))
										|| (assembly.StartsWith("AssemblyTrademark(", StringComparison.OrdinalIgnoreCase))
										|| (assembly.StartsWith("AssemblyCulture(", StringComparison.OrdinalIgnoreCase))
										)
									{
										firstSectionEnd = endIndex + 1;
									}
									startIndex = endIndex + 1;
								}
							}
							else
							{
								startIndex = colonIndex + 1;
							}
						}
						else
						{
							startIndex = assemblyIndex + 1;
						}
					}
				}
				else
				{
					startIndex = -1;
				}
			}
			// not found, add in special cases which might not normally exist
			if (String.Compare(name, "CLSCompliant", StringComparison.OrdinalIgnoreCase) == 0)
			{
				firstSectionEnd = firstSectionEnd + 2;
				assemblyInfoText = assemblyInfoText.Substring(0, firstSectionEnd)
					+ "\r\n"
					+ "// CLS compliance\r\n"
					+ "[assembly: CLSCompliant(" + newValue + ")]\r\n"
					+ assemblyInfoText.Substring(firstSectionEnd);

				if (assemblyInfoText.IndexOf("using System;", StringComparison.OrdinalIgnoreCase) == -1)
				{
					assemblyInfoText = "using System;\r\n" + assemblyInfoText;
				}
			}
			return "";
		}
	}
}
