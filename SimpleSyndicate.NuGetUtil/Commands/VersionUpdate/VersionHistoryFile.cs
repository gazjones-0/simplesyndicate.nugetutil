﻿using System;
using System.Globalization;
using System.Xml;

namespace SimpleSyndicate.NuGetUtil.Commands.VersionUpdate
{
	public class VersionHistoryFile
	{
		public string Path { get; private set; }

		public string FileName { get; private set; }

		private XmlDocument versionHistoryXml;

		public VersionHistoryFile(string file)
		{
			FileName = System.IO.Path.GetFileName(file);
			Path = System.IO.Path.GetDirectoryName(file);
			versionHistoryXml = new XmlDocument();
			versionHistoryXml.Load(Path + System.IO.Path.DirectorySeparatorChar + FileName);
		}

		public string LatestVersionHistoryAsText()
		{
			var namespaceManager = new XmlNamespaceManager(versionHistoryXml.NameTable);
			namespaceManager.AddNamespace("doc", "http://ddue.schemas.microsoft.com/authoring/2003/5");
			var section = versionHistoryXml.SelectSingleNode("//topic//doc:developerConceptualDocument//doc:section", namespaceManager);
			var changes = section.SelectSingleNode("doc:content//doc:list", namespaceManager);
			var changeItems = changes.SelectNodes("doc:listItem", namespaceManager);
			string latestVersionHistory = String.Empty;
			for(int index = 0; index < changeItems.Count; index++)
			{
				var changeItem = changeItems.Item(index).InnerXml;
				changeItem = changeItem.Replace("<codeEntityReference>T:", "<codeEntityReference>");
				changeItem = changeItem.Replace("<codeEntityReference>N:", "<codeEntityReference>");
				changeItem = changeItem.Replace("`1</codeEntityReference>", "</codeEntityReference>");
				changeItem = changeItem.Replace("`2</codeEntityReference>", "</codeEntityReference>");
				changeItem = changeItem.Replace("`3</codeEntityReference>", "</codeEntityReference>");
				changeItem = changeItem.Replace("`4</codeEntityReference>", "</codeEntityReference>");
				changeItem = changeItem.Replace("`5</codeEntityReference>", "</codeEntityReference>");
				var node = new XmlDocument();
				node.InnerXml = changeItem;
				changeItem = node.InnerText.Trim();
				if (!String.IsNullOrEmpty(latestVersionHistory))
				{
					if (latestVersionHistory.EndsWith(".", StringComparison.OrdinalIgnoreCase))
					{
						latestVersionHistory = latestVersionHistory.Substring(0, latestVersionHistory.Length - 1) + "; ";
					}
					else
					{
						latestVersionHistory = latestVersionHistory + "; ";
					}
					latestVersionHistory = latestVersionHistory + changeItem.Substring(0, 1).ToLower(CultureInfo.CurrentCulture) + changeItem.Substring(1);
				}
				else
				{
					latestVersionHistory = changeItem;
				}
			}
			return latestVersionHistory;
		}
	}
}
