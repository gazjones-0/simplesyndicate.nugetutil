﻿using System;

namespace SimpleSyndicate.NuGetUtil.Commands.VersionUpdate
{
	public static class VersionFactory
	{
		public static System.Version Create(NuspecFile nuspecFile)
		{
			if (nuspecFile == null)
			{
				throw new ArgumentNullException("nuspecFile");
			}
			return new System.Version(nuspecFile.Version.Replace("-prerelease", "."));
		}

		public static System.Version IncrementMajorVersion(NuspecFile nuspecFile)
		{
			return IncrementMajorVersion(Create(nuspecFile));
		}

		public static System.Version IncrementMajorVersion(System.Version currentVersion)
		{
			if (currentVersion == null)
			{
				throw new ArgumentNullException("currentVersion");
			}
			return new System.Version(currentVersion.Major + 1, 0, 0);
		}

		public static System.Version IncrementMinorVersion(NuspecFile nuspecFile)
		{
			return IncrementMinorVersion(Create(nuspecFile));
		}

		public static System.Version IncrementMinorVersion(System.Version currentVersion)
		{
			if (currentVersion == null)
			{
				throw new ArgumentNullException("currentVersion");
			}
			return new System.Version(currentVersion.Major, currentVersion.Minor + 1, 0);
		}

		public static System.Version IncrementPointVersion(NuspecFile nuspecFile)
		{
			return IncrementPointVersion(Create(nuspecFile));
		}

		public static System.Version IncrementPointVersion(System.Version currentVersion)
		{
			if (currentVersion == null)
			{
				throw new ArgumentNullException("currentVersion");
			}
			if (currentVersion.Revision == -1)
			{
				return new System.Version(currentVersion.Major, currentVersion.Minor, currentVersion.Build + 1);
			}
			return new System.Version(currentVersion.Major, currentVersion.Minor, currentVersion.Build);
		}

		public static System.Version IncrementPrereleaseVersion(NuspecFile nuspecFile)
		{
			return IncrementPrereleaseVersion(Create(nuspecFile));
		}

		public static System.Version IncrementPrereleaseVersion(System.Version currentVersion)
		{
			if (currentVersion == null)
			{
				throw new ArgumentNullException("currentVersion");
			}
			if (currentVersion.Revision == -1)
			{
				return new System.Version(currentVersion.Major, currentVersion.Minor, currentVersion.Build + 1, 1);
			}
			return new System.Version(currentVersion.Major, currentVersion.Minor, currentVersion.Build, currentVersion.Revision + 1);
		}
	}
}
