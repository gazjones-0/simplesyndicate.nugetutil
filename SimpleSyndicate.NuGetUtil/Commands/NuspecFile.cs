﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace SimpleSyndicate.NuGetUtil.Commands
{
	public class NuspecFile
	{
		public string Path { get; private set; }

		public string FileName { get; private set; }

		private XmlDocument nuspecXml;

		private string SymbolsFilename;

		private XmlDocument nuspecSymbolsXml;

		public NuspecFile(string file)
		{
			FileName = System.IO.Path.GetFileName(file);
			Path = System.IO.Path.GetDirectoryName(file);
			nuspecXml = new XmlDocument();
			nuspecXml.Load(Path + System.IO.Path.DirectorySeparatorChar + FileName);

			SymbolsFilename = FileName.Replace(".nuspec", ".Symbols.nuspec");
			if (System.IO.File.Exists(Path + "\\" + SymbolsFilename))
			{
				nuspecSymbolsXml = new XmlDocument();
				nuspecSymbolsXml.Load(Path + System.IO.Path.DirectorySeparatorChar + SymbolsFilename);
			}
		}

		public string Authors { get { return GetMetadata("authors"); } set { SetMetadata("authors", value); } }

		public string Copyright { get { return GetMetadata("copyright"); } set { SetMetadata("copyright", value); } }

		public string Description { get { return GetMetadata("description"); } set { SetMetadata("description", value); } }

		public string Owners { get { return GetMetadata("owners"); } set { SetMetadata("owners", value); } }

		public Uri ProjectUrl { get { return new Uri(GetMetadata("projectUrl")); } set { SetMetadata("projectUrl", value == null ? "?" : value.ToString()); } }

		public string ReleaseNotes { get { return GetMetadata("releaseNotes"); } set { SetMetadata("releaseNotes", value); } }

		public string RequireLicenseAcceptance { get { return GetMetadata("requireLicenseAcceptance"); } set { SetMetadata("requireLicenseAcceptance", value); } }

		public string Summary { get { return GetMetadata("summary"); } set { SetMetadata("summary", value); } }

		public string Tags { get { return GetMetadata("tags"); } set { SetMetadata("tags", value); } }

		public string Version { get { return GetMetadata("version"); } set { SetMetadata("version", value); } }

		public void SetDependencies(IList<NuGetPackage> dependencies)
		{
			if (dependencies == null)
			{
				throw new ArgumentNullException("dependencies");
			}
			var metadataNode = nuspecXml.SelectSingleNode("//package/metadata");
			var dependenciesNode = metadataNode.SelectSingleNode("//dependencies");
			if (dependenciesNode != null)
			{
				metadataNode.RemoveChild(dependenciesNode);
			}
			dependenciesNode = nuspecXml.CreateElement("dependencies");
			foreach (var dependency in dependencies)
			{
				var dependencyNode = nuspecXml.CreateElement("dependency");
				var id = nuspecXml.CreateAttribute("id");
				id.Value = dependency.Id;
				dependencyNode.Attributes.Append(id);

				var version = nuspecXml.CreateAttribute("version");
				version.Value = dependency.Version;
				dependencyNode.Attributes.Append(version);

				dependenciesNode.AppendChild(dependencyNode);
			}
			metadataNode.AppendChild(dependenciesNode);
			SaveNuspec();

			if (nuspecSymbolsXml != null)
			{
				var symbolsMetadataNode = nuspecSymbolsXml.SelectSingleNode("//package/metadata");
				var symbolsDependenciesNode = symbolsMetadataNode.SelectSingleNode("//dependencies");
				if (symbolsDependenciesNode != null)
				{
					symbolsMetadataNode.RemoveChild(symbolsDependenciesNode);
				}
				symbolsDependenciesNode = nuspecSymbolsXml.CreateElement("dependencies");
				foreach (var dependency in dependencies)
				{
					var dependencyNode = nuspecSymbolsXml.CreateElement("dependency");
					var id = nuspecSymbolsXml.CreateAttribute("id");
					id.Value = dependency.Id;
					dependencyNode.Attributes.Append(id);

					var version = nuspecSymbolsXml.CreateAttribute("version");
					version.Value = dependency.Version;
					dependencyNode.Attributes.Append(version);

					symbolsDependenciesNode.AppendChild(dependencyNode);
				}
				symbolsMetadataNode.AppendChild(symbolsDependenciesNode);
				SaveNuspecSymbols();
			}
		}

		private string GetMetadata(string name)
		{
			return nuspecXml.SelectSingleNode("//package/metadata/" + name).InnerText;
		}

		private void SetMetadata(string name, string value)
		{
			nuspecXml.SelectSingleNode("//package/metadata/" + name).InnerText = value;
			SaveNuspec();
			if (nuspecSymbolsXml != null)
			{
				nuspecSymbolsXml.SelectSingleNode("//package/metadata/" + name).InnerText = value;
				SaveNuspecSymbols();
			}
		}

		private void SaveNuspec()
		{
			ExecuteOperation.Execute(() => { nuspecXml.Save(Path + System.IO.Path.DirectorySeparatorChar + FileName); });
		}

		private void SaveNuspecSymbols()
		{
			ExecuteOperation.Execute(() => { nuspecSymbolsXml.Save(Path + System.IO.Path.DirectorySeparatorChar + SymbolsFilename); });
		}
	}
}
