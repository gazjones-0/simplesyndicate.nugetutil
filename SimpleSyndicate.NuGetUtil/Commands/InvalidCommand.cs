﻿using System;
using System.Globalization;

namespace SimpleSyndicate.NuGetUtil.Commands
{
	public static class InvalidCommand
	{
		public static void Execute(string[] args)
		{
			if (args == null)
			{
				throw new ArgumentNullException("args");
			}
			if (!Program.HasCommandBeenExecuted())
			{
				Program.OutputError(String.Format(CultureInfo.CurrentCulture, "Invalid command \"{0}\"", args[0]));
			}
		}
	}
}
