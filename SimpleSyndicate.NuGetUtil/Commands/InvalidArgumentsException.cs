﻿using System;

namespace SimpleSyndicate.NuGetUtil.Commands
{
	[Serializable]
	public class InvalidArgumentsException : Exception
	{
		public InvalidArgumentsException()
			: base()
		{
		}

		public InvalidArgumentsException(string message)
			: base(message)
		{
		}

		public InvalidArgumentsException(string message, Exception innerException)
			: base(message, innerException)
		{
		}

		protected InvalidArgumentsException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
			: base(info, context)
		{
		}
	}
}
