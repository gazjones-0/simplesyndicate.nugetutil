﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Xml;

namespace SimpleSyndicate.NuGetUtil.Commands.DependenciesUpdate
{
	public static class NuGetPackageDependenciesHelpers
	{
		private static IList<NuGetPackage> packages;

		public static void GenerateDependencies(NuGetPackage package, string pathToProject)
		{
			CreatePackagesList(pathToProject);
			GenerateDependenciesInternal(package, pathToProject);
		}

		private static void CreatePackagesList(string pathToProject)
		{
			if (packages != null)
			{
				return;
			}
			packages = new List<NuGetPackage>();
			var solutionDir = System.IO.Directory.GetParent(pathToProject);
			var packagesDir = new DirectoryInfo(solutionDir.FullName + "\\packages\\");
			foreach (var subdirectory in packagesDir.GetDirectories())
			{
				if (File.Exists(subdirectory.FullName + "\\" + subdirectory.Name + ".nupkg"))
				{
					using(var zipArchive = ZipFile.OpenRead(subdirectory.FullName + "\\" + subdirectory.Name + ".nupkg"))
					{
						var nuspec = GetNuspecFromNupkg(zipArchive);
						var package = new NuGetPackage();
						package.Id = GetIdFromNuspec(nuspec);
						package.Version = GetVersionFromNuspec(nuspec);
						package.Path = subdirectory.FullName;
						packages.Add(package);
					}
				}
			}
		}

		private static void GenerateDependenciesInternal(NuGetPackage package, string pathToProject)
		{
			if (PackageMustBeProcessed(package))
			{
				var nuspec = GetPackageNuspec(package);
				var dependencies = nuspec.SelectNodes("//*[local-name()='package']//*[local-name()='metadata']//*[local-name()='dependencies']//*[local-name()='dependency']");
				for (int i = 0; i < dependencies.Count; i++)
				{
					var dependency = new NuGetPackage();
					dependency.Id = dependencies.Item(i).Attributes["id"].Value;
					dependency.Version = dependencies.Item(i).Attributes["version"].Value;
					package.Dependencies.Add(dependency);
					GenerateDependenciesInternal(dependency, pathToProject);
				}
			}
		}

		private static bool PackageMustBeProcessed(NuGetPackage package)
		{
			var possiblePackages = packages.Where(x => x.Id == package.Id);
			if (possiblePackages.Count() == 0)
			{
				if ((String.Compare(package.Id, "Microsoft.BCL", StringComparison.OrdinalIgnoreCase) == 0)
					|| (String.Compare(package.Id, "Microsoft.CSharp", StringComparison.OrdinalIgnoreCase) == 0)
					|| (package.Id.StartsWith("System.", StringComparison.OrdinalIgnoreCase)))
				{
					return false;
				}
			}
			return true;
		}

		private static XmlDocument GetPackageNuspec(NuGetPackage package)
		{
			return GetNuspecFromNupkg(GetPackage(package));
		}

		private static ZipArchive GetPackage(NuGetPackage package)
		{
			var possiblePackages = packages.Where(x => x.Id == package.Id);
			if (possiblePackages.Count() == 0)
			{
				throw new InvalidOperationException();
			}
			if (possiblePackages.Count() == 1)
			{
				return ZipFile.OpenRead(possiblePackages.First().Path + "\\" + possiblePackages.First().Id + "." + possiblePackages.First().Version + ".nupkg");
			}
			NuGetPackage foundPackage = null;
			foreach (var possiblePackage in possiblePackages)
			{
				if (foundPackage == null)
				{
					foundPackage = possiblePackage;
				}
				else
				{
					var foundVersion = new System.Version(GetSanitizedVersion(foundPackage));
					var possibleVersion = new System.Version(GetSanitizedVersion(possiblePackage));
					if (possibleVersion < foundVersion)
					{
						foundPackage = possiblePackage;
					}
				}
			}
			return ZipFile.OpenRead(foundPackage.Path + "\\" + foundPackage.Id + "." + foundPackage.Version + ".nupkg");
		}

		public static string GetSanitizedVersion(NuGetPackage package)
		{
			if (package == null)
			{
				throw new ArgumentNullException("package");
			}
			var version = package.Version;
			if (version.StartsWith("[", StringComparison.OrdinalIgnoreCase))
			{
				version = version.Substring(1);
				version = version.Substring(0, version.IndexOf(",", StringComparison.OrdinalIgnoreCase));
			}
			if (version.StartsWith("(", StringComparison.OrdinalIgnoreCase))
			{
				version = version.Substring(1);
				version = version.Substring(0, version.IndexOf(",", StringComparison.OrdinalIgnoreCase));
			}
			return version.Trim();
		}

		private static XmlDocument GetNuspecFromNupkg(ZipArchive nupkg)
		{
			var buffer = GetNuspecAsBufferFromNuPkg(nupkg);
			var nuspec = GetNuspecAsASCII(buffer);
			if (!nuspec.StartsWith("<?xml", StringComparison.OrdinalIgnoreCase))
			{
				nuspec = GetNuspecAsBigEndianUnicode(buffer);
			}
			if (!nuspec.StartsWith("<?xml", StringComparison.OrdinalIgnoreCase))
			{
				nuspec = GetNuspecAsUnicode(buffer);
			}
			if (!nuspec.StartsWith("<?xml", StringComparison.OrdinalIgnoreCase))
			{
				nuspec = GetNuspecAsUTF32(buffer);
			}
			if (!nuspec.StartsWith("<?xml", StringComparison.OrdinalIgnoreCase))
			{
				nuspec = GetNuspecAsUTF7(buffer);
			}
			if (!nuspec.StartsWith("<?xml", StringComparison.OrdinalIgnoreCase))
			{
				nuspec = GetNuspecAsUTF8(buffer);
			}
			if (nuspec.StartsWith("<?xml", StringComparison.OrdinalIgnoreCase))
			{
				var nuspecXml = new XmlDocument();
				nuspecXml.LoadXml(nuspec);
				return nuspecXml;
			}
			throw new InvalidOperationException();
		}

		private static string GetNuspecAsASCII(byte[] buffer)
		{
			var nuspec = Encoding.ASCII.GetString(buffer);
			if (nuspec.StartsWith(Encoding.ASCII.GetString(Encoding.ASCII.GetPreamble()), StringComparison.OrdinalIgnoreCase))
			{
				nuspec = nuspec.Remove(0, Encoding.ASCII.GetString(Encoding.ASCII.GetPreamble()).Length);
			}
			return nuspec;
		}

		private static string GetNuspecAsBigEndianUnicode(byte[] buffer)
		{
			var nuspec = Encoding.BigEndianUnicode.GetString(buffer);
			if (nuspec.StartsWith(Encoding.BigEndianUnicode.GetString(Encoding.BigEndianUnicode.GetPreamble()), StringComparison.OrdinalIgnoreCase))
			{
				nuspec = nuspec.Remove(0, Encoding.BigEndianUnicode.GetString(Encoding.BigEndianUnicode.GetPreamble()).Length);
			}
			return nuspec;
		}

		private static string GetNuspecAsUnicode(byte[] buffer)
		{
			var nuspec = Encoding.Unicode.GetString(buffer);
			if (nuspec.StartsWith(Encoding.Unicode.GetString(Encoding.Unicode.GetPreamble()), StringComparison.OrdinalIgnoreCase))
			{
				nuspec = nuspec.Remove(0, Encoding.Unicode.GetString(Encoding.Unicode.GetPreamble()).Length);
			}
			return nuspec;
		}

		private static string GetNuspecAsUTF32(byte[] buffer)
		{
			var nuspec = Encoding.UTF32.GetString(buffer);
			if (nuspec.StartsWith(Encoding.UTF32.GetString(Encoding.UTF32.GetPreamble()), StringComparison.OrdinalIgnoreCase))
			{
				nuspec = nuspec.Remove(0, Encoding.UTF32.GetString(Encoding.UTF32.GetPreamble()).Length);
			}
			return nuspec;
		}

		private static string GetNuspecAsUTF7(byte[] buffer)
		{
			var nuspec = Encoding.UTF7.GetString(buffer);
			if (nuspec.StartsWith(Encoding.UTF7.GetString(Encoding.UTF7.GetPreamble()), StringComparison.OrdinalIgnoreCase))
			{
				nuspec = nuspec.Remove(0, Encoding.UTF7.GetString(Encoding.UTF7.GetPreamble()).Length);
			}
			return nuspec;
		}

		private static string GetNuspecAsUTF8(byte[] buffer)
		{
			var nuspec = Encoding.UTF8.GetString(buffer);
			if (nuspec.StartsWith(Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble()), StringComparison.OrdinalIgnoreCase))
			{
				nuspec = nuspec.Remove(0, Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble()).Length);
			}
			return nuspec;
		}

		private static byte[] GetNuspecAsBufferFromNuPkg(ZipArchive nupkg)
		{
			var zipEntry = nupkg.GetEntry(GetNuspecEntryNameFromNupkg(nupkg));
			var zipStream = zipEntry.Open();
			var buffer = new byte[zipEntry.Length];
			zipStream.Read(buffer, 0, Convert.ToInt32(zipEntry.Length));
			zipStream.Close();
			return buffer;
		}

		private static string GetNuspecEntryNameFromNupkg(ZipArchive nupkg)
		{
			foreach (var entry in nupkg.Entries)
			{
				if (entry.Name.EndsWith(".nuspec", StringComparison.OrdinalIgnoreCase))
				{
					return entry.FullName;
				}
			}
			throw new InvalidOperationException();
		}

		private static string GetIdFromNuspec(XmlDocument nuspec)
		{
			return nuspec.SelectSingleNode("//*[local-name()='package']//*[local-name()='metadata']//*[local-name()='id']").InnerText;
		}

		private static string GetVersionFromNuspec(XmlDocument nuspec)
		{
			return nuspec.SelectSingleNode("//*[local-name()='package']//*[local-name()='metadata']//*[local-name()='version']").InnerText;
		}
	}
}
