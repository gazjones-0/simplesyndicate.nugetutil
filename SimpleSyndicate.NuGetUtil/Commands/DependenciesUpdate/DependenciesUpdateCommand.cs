﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace SimpleSyndicate.NuGetUtil.Commands.DependenciesUpdate
{
	public static class DependenciesUpdateCommand
	{
		public static void Execute(string[] args)
		{
			if (args == null)
			{
				throw new ArgumentNullException("args");
			}
			if (CommandUtility.IsCommand(args, "DependenciesUpdate"))
			{
				Program.CommandExecuted();
				if (args.Count() > 1)
				{
					Update(args[1]);
					return;
				}
				Update();
			}
		}

		public static void Update()
		{
			Update(System.Environment.CurrentDirectory);
		}

		public static void Update(string path)
		{
			Program.OutputInfo("Updating .nuspec dependencies");
			var packages = GeneratePackagesWithDependencies(path);
			MinimizeDependencies(packages);
			var nuspecFile = NuspecFileFactory.Open(path, true);
			nuspecFile.SetDependencies(packages);
			Program.OutputInfo("Updated .nuspec file");
		}

		private static IList<NuGetPackage> GeneratePackagesWithDependencies(string path)
		{
			Program.OutputInfo(String.Format(CultureInfo.CurrentCulture, "Path is {0}", path));
			var packagesConfig = NuGetPackagesConfigFileFactory.Create(path);
			var packages = new List<NuGetPackage>();
			foreach (var package in packagesConfig.Packages)
			{
				NuGetPackageDependenciesHelpers.GenerateDependencies(package, path);
				packages.Add(package);
			}
			return packages;
		}

		private static void MinimizeDependencies(IList<NuGetPackage> packages)
		{
			bool changes = true;
			while (changes == true)
			{
				changes = MinimizeDependenciesInternal(packages);
			}
		}

		private static bool MinimizeDependenciesInternal(IList<NuGetPackage> packages)
		{
			foreach (var package in packages)
			{
				foreach (var packageBeingChecked in packages)
				{
					if (!IsSamePackage(package, packageBeingChecked))
					{
						if (IsInPackageDependencies(packageBeingChecked, package))
						{
							packages.Remove(package);
							return true;
						}
					}
				}
			}
			return false;
		}

		private static bool IsSamePackage(NuGetPackage package1, NuGetPackage package2)
		{
			if ((package1.Id == package2.Id) && (package1.Version == package2.Version))
			{
				return true;
			}
			return false;
		}

		private static bool IsLowerVersionPackage(NuGetPackage package1, NuGetPackage package2)
		{
			if ((package1.Id == package2.Id) && (new System.Version(NuGetPackageDependenciesHelpers.GetSanitizedVersion(package1)) <= new System.Version(package2.Version)))
			{
				return true;
			}
			return false;
		}

		private static bool IsInPackageDependencies(NuGetPackage packageToCheck, NuGetPackage packageToFind)
		{
			return IsInPackageDependenciesInternal(packageToCheck.Dependencies, packageToFind);
		}

		private static bool IsInPackageDependenciesInternal(IList<NuGetPackage> dependencies, NuGetPackage packageToFind)
		{
			foreach (var package in dependencies)
			{
				if (IsSamePackage(package, packageToFind))
				{
					return true;
				}
				if (IsLowerVersionPackage(package, packageToFind))
				{
					return true;
				}
				var isInDependencies = IsInPackageDependenciesInternal(package.Dependencies, packageToFind);
				if (isInDependencies)
				{
					return true;
				}
			}
			return false;
		}
	}
}
