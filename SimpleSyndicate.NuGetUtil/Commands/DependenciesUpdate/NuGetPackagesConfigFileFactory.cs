﻿using System;

namespace SimpleSyndicate.NuGetUtil.Commands.DependenciesUpdate
{
	public static class NuGetPackagesConfigFileFactory
	{
		public static NuGetPackagesConfigFile Create()
		{
			return Create(System.Environment.CurrentDirectory);
		}

		public static NuGetPackagesConfigFile Create(string path)
		{
			foreach (var file in System.IO.Directory.EnumerateFiles(path))
			{
				if (file.EndsWith("\\packages.config", StringComparison.OrdinalIgnoreCase))
				{
					return new NuGetPackagesConfigFile(file);
				}
			}
			throw new InvalidOperationException();
		}
	}
}
