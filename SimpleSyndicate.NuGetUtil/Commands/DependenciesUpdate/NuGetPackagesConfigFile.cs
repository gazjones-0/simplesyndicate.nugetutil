﻿using System.Collections.Generic;
using System.Xml;

namespace SimpleSyndicate.NuGetUtil.Commands.DependenciesUpdate
{
	public class NuGetPackagesConfigFile
	{
		public string Path { get; private set; }

		public string FileName { get; private set; }

		private XmlDocument packagesXml;

		public NuGetPackagesConfigFile(string file)
		{
			FileName = System.IO.Path.GetFileName(file);
			Path = System.IO.Path.GetDirectoryName(file);
			packagesXml = new XmlDocument();
			packagesXml.Load(Path + System.IO.Path.DirectorySeparatorChar + FileName);
		}

		public IList<NuGetPackage> Packages
		{
			get
			{
				var nodes = packagesXml.SelectNodes("//packages/package");
				var list = new List<NuGetPackage>();
				for (int i = 0; i < nodes.Count; i++)
				{
					var package = new NuGetPackage();
					package.Id = nodes.Item(i).Attributes["id"].Value;
					package.Version = nodes.Item(i).Attributes["version"].Value;
					list.Add(package);
				}
				return list;
			}
		}
	}
}
