﻿using System;
using System.IO;
using System.Linq;

namespace SimpleSyndicate.NuGetUtil.Commands.Replace
{
	public static class ReplaceCommand
	{
		public static void Execute(string[] args)
		{
			if (args == null)
			{
				throw new ArgumentNullException("args");
			}
			if (CommandUtility.IsCommand(args, "Replace"))
			{
				Program.CommandExecuted();
				if (args.Count() < 2)
				{
					Program.OutputError("No file specified to perform string replacement on");
					return;
				}
				if (args.Count() < 3)
				{
					Program.OutputError("No old value to find specified");
					return;
				}
				if (args.Count() < 4)
				{
					Program.OutputError("No new value to replace specified");
					return;
				}
				ReplaceString(args[1], args[2], args[3]);
			}
		}

		public static void ReplaceString(string file, string oldValue, string newValue)
		{
			var contents = File.ReadAllText(file);
			contents = contents.Replace(oldValue, newValue);
			ExecuteOperation.Execute(() => { File.WriteAllText(file, contents); });
		}
	}
}
