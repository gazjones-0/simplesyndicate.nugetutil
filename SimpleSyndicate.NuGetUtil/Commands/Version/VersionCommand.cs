﻿using System.Reflection;

namespace SimpleSyndicate.NuGetUtil.Commands.Version
{
	public static class VersionCommand
	{
		public static void Execute(string[] args)
		{
			if (CommandUtility.IsCommand(args, "Version"))
			{
				Program.CommandExecuted();
				OutputVersion();
			}
		}

		public static void OutputVersion()
		{
			Program.OutputInfo(Assembly.GetExecutingAssembly().GetName().Version.ToString(3));
		}
	}
}
