﻿using System;
using System.Collections.Generic;
using System.IO;

namespace SimpleSyndicate.NuGetUtil.Commands
{
	public class VisualStudioProjectFile
	{
		public string FileName { get; private set; }

		public string Path { get; private set; }

		public string ProjectName { get; private set; }

		public VisualStudioSolutionFile SolutionFile { get; private set; }

		private string originalLines = null;

		private List<string> lines = null;

		public VisualStudioProjectFile(string path)
		{
			FileName = DetermineFileName(path);
			Path = Directory.GetParent(FileName).FullName;
			var file = new FileInfo(FileName);
			ProjectName = file.Name.Substring(0, file.Name.Length - file.Extension.Length);
			SolutionFile = new VisualStudioSolutionFile(Path);
		}

		private void LoadProjectFile()
		{
			lines = new List<string>(File.ReadAllLines(FileName));
			originalLines = String.Join("\n", lines);
		}

		private void SaveProjectFile()
		{
			if (lines == null)
			{
				throw new InvalidOperationException("Can't modify Visual Studio project file as it was not loaded.");
			}
			if (String.Compare(originalLines, String.Join("\n", lines), StringComparison.Ordinal) != 0)
			{
				ExecuteOperation.Execute(() => { File.WriteAllLines(FileName, lines); });
			}
		}

		public void AddReference(string include, string hintPath, bool isPrivate)
		{
			LoadProjectFile();
			var index = AddItemGroupIfRequired("Reference");
			AddReferenceToGroup(index, include, hintPath, isPrivate);
			SaveProjectFile();
		}

		public void AddGlobalProperty(string name, string value)
		{
			LoadProjectFile();
			AddProperty(null, name, value);
			SaveProjectFile();
		}

		public void AddDebugProperty(string name, string value)
		{
			LoadProjectFile();
			AddProperty("Debug", name, value);
			SaveProjectFile();
		}

		public void AddReleaseProperty(string name, string value)
		{
			LoadProjectFile();
			AddProperty("Release", name, value);
			SaveProjectFile();
		}

		public void AddImportProject(string project)
		{
			LoadProjectFile();
			AddImport("Project", project);
			SaveProjectFile();
		}

		public void AddItemBuildTypeNone(string itemName)
		{
			LoadProjectFile();
			var index = AddItemGroupIfRequired("None", "Content");
			AddItemToGroup(index, "None", itemName, null, null);
			SaveProjectFile();
		}

		public void AddItemBuildTypeNone(string itemName, string subtype)
		{
			LoadProjectFile();
			var index = AddItemGroupIfRequired("None", "Content");
			AddItemToGroup(index, "None", itemName, subtype, null);
			SaveProjectFile();
		}

		public void AddItemBuildTypeNone(string itemName, string subtype, string dependentUpon)
		{
			LoadProjectFile();
			var index = AddItemGroupIfRequired("None", "Content");
			AddItemToGroup(index, "None", itemName, subtype, dependentUpon);
			SaveProjectFile();
		}

		public void AddItemBuildTypeCodeAnalysisDictionary(string itemName)
		{
			LoadProjectFile();
			var index = AddItemGroupIfRequired("CodeAnalysisDictionary");
			AddItemToGroup(index, "CodeAnalysisDictionary", itemName, null, null);
			SaveProjectFile();
		}

		public void AddItemBuildTypeCompile(string itemName)
		{
			LoadProjectFile();
			var index = AddItemGroupIfRequired("Compile");
			AddItemToGroup(index, "Compile", itemName, null, null);
			SaveProjectFile();
		}

		public void AddItemBuildTypeContent(string itemName)
		{
			LoadProjectFile();
			var index = AddItemGroupIfRequired("None", "Content");
			AddItemToGroup(index, "Content", itemName, null, null);
			SaveProjectFile();
		}

		public void RemoveItemBuildTypeCompile(string itemName)
		{
			LoadProjectFile();
			RemoveItem(itemName, "Compile");
			SaveProjectFile();
		}

		private void AddImport(string type, string value)
		{
			int index = 0;
			while (index < lines.Count)
			{
				if (lines[index].IndexOf("<Import " + type + "=\"" + value + "\"") >= 0)
				{
					return;
				}
				if (lines[index].IndexOf("</Project>") >= 0)
				{
					lines.Insert(index, "  <Import Project=\"" + value + "\" />");
					return;
				}
				index++;
			}
		}

		private void AddProperty(string type, string name, string value)
		{
			var index = FindPropertyGroup(type);
			while (index >= 0)
			{
				AddPropertyToGroup(index, name, value);
				index = FindPropertyGroup(type, index + 1);
			}
		}

		private int FindPropertyGroup(string type)
		{
			return FindPropertyGroup(type, 0);
		}

		private int FindPropertyGroup(string type, int index)
		{
			while (index < lines.Count)
			{
				if (String.IsNullOrWhiteSpace(type))
				{
					if (lines[index].IndexOf("<PropertyGroup>", StringComparison.OrdinalIgnoreCase) >= 0)
					{
						return index;
					}
				}
				else
				{
					if ((lines[index].IndexOf("<PropertyGroup", StringComparison.OrdinalIgnoreCase) >= 0)
						&& (lines[index].IndexOf("'" + type, StringComparison.OrdinalIgnoreCase) >= 0))
					{
						return index;
					}
				}
				index++;
			}
			return -1;
		}

		private void AddPropertyToGroup(int index, string name, string value)
		{
			while (lines[index].IndexOf("</PropertyGroup>", StringComparison.OrdinalIgnoreCase) < 0)
			{
				var start = lines[index].IndexOf("<" + name + ">", StringComparison.OrdinalIgnoreCase);
				if (start > 0)
				{
					var end = lines[index].IndexOf("</" + name + ">", start, StringComparison.OrdinalIgnoreCase);
					if (end > 0)
					{
						lines[index] = lines[index].Substring(0, start) + "<" + name + ">" + value + lines[index].Substring(end);
					}
					return;
				}
				index++;
			}
			if (String.IsNullOrWhiteSpace(value))
			{
				lines.Insert(index, "    <" + name + " />");
			}
			else
			{
				lines.Insert(index, "    <" + name + ">" + value + "</" + name + ">");
			}
		}

		private void AddItemToGroup(int index, string type, string itemName, string subType, string dependentUpon)
		{
			while (lines[index].IndexOf("</ItemGroup>", StringComparison.OrdinalIgnoreCase) < 0)
			{
				if (lines[index].IndexOf("Include=\"" + itemName + "\"", StringComparison.OrdinalIgnoreCase) > 0)
				{
					lines[index] = lines[index].Replace("<None ", "<" + type + " ");
					lines[index] = lines[index].Replace("<Content ", "<" + type + " ");
					index++;
					if (lines[index].IndexOf("<SubType>", StringComparison.OrdinalIgnoreCase) >= 0)
					{
						index--;
						lines[index] = lines[index].Replace(">", " />");
						index++;
						lines.RemoveAt(index);
						while(lines[index].IndexOf("</None>", StringComparison.OrdinalIgnoreCase) < 0)
						{
							lines.RemoveAt(index);
						}
						lines.RemoveAt(index);
					}
					return;
				}
				index++;
			}
			if (!String.IsNullOrWhiteSpace(subType))
			{
				lines.Insert(index, "    </" + type + ">");
				lines.Insert(index, "      <SubType>" + subType + "</SubType>");
				lines.Insert(index, "    <" + type + " Include=\"" + itemName + "\">");
			}
			else
			{
				if (!String.IsNullOrWhiteSpace(dependentUpon))
				{
					lines.Insert(index, "    </" + type + ">");
					lines.Insert(index, "      <DependentUpon>" + dependentUpon + "</DependentUpon>");
					lines.Insert(index, "    <" + type + " Include=\"" + itemName + "\">");
				}
				else
				{
					lines.Insert(index, "    <" + type + " Include=\"" + itemName + "\" />");
				}
			}
		}

		private void AddReferenceToGroup(int index, string include, string hintPath, bool? isPrivate)
		{
			while (lines[index].IndexOf("</ItemGroup>", StringComparison.OrdinalIgnoreCase) < 0)
			{
				if (lines[index].IndexOf("Include=\"" + include + "\"", StringComparison.OrdinalIgnoreCase) > 0)
				{
					return;
				}
				index++;
			}
			if (String.IsNullOrWhiteSpace(hintPath) && isPrivate == null)
			{
				lines.Insert(index, "    <Reference Include=\"" + include + "\" />");
			}
			else
			{
				lines.Insert(index, "    </Reference>");
				if (isPrivate != null)
				{
					lines.Insert(index, "      <Private>" + isPrivate.ToString() + "</Private>");
				}
				if (!String.IsNullOrWhiteSpace(hintPath))
				{
					lines.Insert(index, "      <HintPath>" + hintPath + "</HintPath>");
				}
				lines.Insert(index, "    <Reference Include=\"" + include + "\">");
			}
		}

		private int AddItemGroupIfRequired(string type)
		{
			return AddItemGroupIfRequired(type, null);
		}

		private int AddItemGroupIfRequired(string type1, string type2)
		{
			var index = ItemGroupExists(type1, type2);
			if (index >= 0)
			{
				return index;
			}
			index = LastItemGroup();
			lines.Insert(index, "  </ItemGroup>");
			lines.Insert(index, "  <ItemGroup>");
			return index;
		}

		private int ItemGroupExists(string type1, string type2)
		{
			int index = 0;
			while (index < lines.Count)
			{
				if (lines[index].IndexOf("<" + type1, System.StringComparison.OrdinalIgnoreCase) >= 0)
				{
					return index;
				}
				if (!String.IsNullOrWhiteSpace(type2))
				{
					if (lines[index].IndexOf("<" + type2, System.StringComparison.OrdinalIgnoreCase) >= 0)
					{
						return index;
					}
				}
				index++;
			}
			return -1;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "ItemGroup", Justification = "Literal is spelt correctly.")]
		private int LastItemGroup()
		{
			int index = lines.Count - 1;
			while(index >= 0)
			{
				if (
					(lines[index].IndexOf("</ItemGroup>", System.StringComparison.OrdinalIgnoreCase) >= 0)
					|| (lines[index].IndexOf("<ItemGroup />", System.StringComparison.OrdinalIgnoreCase) >= 0)
					)
				{
					while(lines[index].Trim().StartsWith("</"))
					{
						index++;
					}
					return index;
				}
				index--;
			}
			throw new InvalidOperationException("Can't find ItemGroup in Visual Studio project file.");
		}

		private static string DetermineFileName(string path)
		{
			if (File.Exists(path))
			{
				return path;
			}
			foreach (var file in Directory.EnumerateFiles(path))
			{
				if (file.EndsWith(".csproj", System.StringComparison.OrdinalIgnoreCase))
				{
					return file;
				}
			}
			throw new FileNotFoundException("Couldn't find Visual Studio project file in " + path);
		}

		private void RemoveItem(string itemName, string type)
		{
			int index = 0;
			while (index < lines.Count)
			{
				if (lines[index].IndexOf("<" + type + " ", System.StringComparison.OrdinalIgnoreCase) >= 0)
				{
					if (lines[index].IndexOf(" Include=\"" + itemName + "\"", System.StringComparison.OrdinalIgnoreCase) >= 0)
					{
						lines.RemoveAt(index);
						return;
					}
				}
				index++;
			}
		}
	}
}
