echo Release checklist:
echo 1. Removed and sorted usings?
echo 2. Performed code analysis?
echo 3. Added a new version history item to help file project?
echo 4. Built help file to ensure it has no errors or warnings?
choice /m "Checklist completed? [Y]es or [N]o?"
if errorlevel 2 goto :end
