rem build the solution
pushd .
cd ..
msbuild /m /nologo /verbosity:quiet /p:Configuration=Release
popd

rem create a sub-folder to perform the packaging as we want to customise the files in it
pushd .
mkdir .nugettemp
copy *.nuspec .nugettemp
cd .nugettemp

rem build release package and push to nuget
nuget pack $project$.nuspec
nuget push *.nupkg$:pushNuGetServer$

rem build symbols package and push to nuget
nuget pack $project$.Symbols.nuspec -Symbols
nuget push *.symbols.nupkg$:pushNuGetServer$

rem back to original directory
popd

rem clean up packaging folder
rmdir /s /q .nugettemp
