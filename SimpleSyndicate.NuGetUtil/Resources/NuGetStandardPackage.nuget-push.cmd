@echo off
rem check nugetutil is available
if not exist ..\tools\SimpleSyndicate.NuGetUtil.exe (
	echo Fatal: ..\tools\SimpleSyndicate.NuGetUtil.exe not found
	goto :end
)

rem check input argument
for /F "delims=" %%i in ('..\tools\SimpleSyndicate.NuGetUtil.exe checkversionupdatearg %1') do set nugetutil=%%i
set nugetutilprefix=%nugetutil:~0,5%
if /i "%nugetutilprefix%" == "Valid" (
	goto :precheck
)
echo Fatal: No version update component specified
echo Usage: nuget-push ^<major^|minor^|point^|prerelease^>
goto :end

:precheck
$:PreCheck$

:checkTools
rem check Visual Studio is available
if not defined VSINSTALLDIR (
	echo Fatal: Visual Studio not found
	goto :end
)

rem check nuget is available
for /F "delims=" %%i in ('nuget') do set nugetversion=%%i
if not defined nugetversion (
	echo Fatal: nuget not found
	goto :end
)

$:checkToolsGit$

:push
rem update version
..\tools\SimpleSyndicate.NuGetUtil versionupdate %1

$:pushUpdateDependencies$

$:pushBuildAndPush$

$:pushBuildHelp$

$:pushGitTags$$:pushGitHelpTags$

$:pushGitCommit$

$:pushGitCommitHelpSolution$

$:pushGitCommitHelp$

:end
