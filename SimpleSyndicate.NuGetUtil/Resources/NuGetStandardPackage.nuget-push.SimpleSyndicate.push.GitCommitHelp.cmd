rem commit the updated documentation
pushd .
cd ..\..\gazooka_g72.bitbucket.org
git add --all *
git commit -a -m "%helpmessage%"
git tag -a %helptag% -m "%helpmessage%"
git push --all
git push --tags
popd
