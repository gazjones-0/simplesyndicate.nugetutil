rem check git is available
for /F "delims=" %%i in ('git --version') do set gitversion=%%i
if not defined gitversion (
	echo Fatal: git not found
	goto :end
)

rem if %home% isn't defined, set it to the home drive and path
if not defined HOME (
	set HOME=%HOMEDRIVE%%HOMEPATH%
)

rem if the home drive and path aren't defined, home will still be undefined so set it to the user profile
if not defined HOME (
	set HOME=%USERPROFILE%
)

rem sanity check %home% is defined, otherwise git won't be able to find its global config
if not defined HOME (
	echo Fatal: No HOME environment variable defined
	echo Fatal: Without this git won't be able to find its global config
	goto :end
)
