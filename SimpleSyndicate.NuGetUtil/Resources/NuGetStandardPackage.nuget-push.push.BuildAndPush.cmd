rem build release package and push to nuget
nuget pack $project$.csproj -Prop Configuration=Release -Build
nuget push *.nupkg$:pushNuGetServer$

rem build symbols package and push to nuget
nuget pack $project$.csproj -Prop Configuration=Release -Symbols
nuget push *.symbols.nupkg$:pushNuGetServer$

rem remove the packages as we don't need them after they've been pushed, and we don't want to commit them to source control
del *.nupkg
