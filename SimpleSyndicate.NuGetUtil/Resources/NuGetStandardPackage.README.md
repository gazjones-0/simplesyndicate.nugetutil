# README #

$project$ NuGet package.

Get the package from https://www.nuget.org/packages/$project$

### What is this repository for? ###

* $summary$

### How do I get started? ###

* See the documentation at $documentationLink$

### Reporting issues ###

* Use the tracker at $trackerLink$
