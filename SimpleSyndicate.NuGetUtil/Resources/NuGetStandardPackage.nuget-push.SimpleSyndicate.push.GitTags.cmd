rem work out the git tags and messages
for /F "delims=" %%i in ('..\tools\SimpleSyndicate.NuGetUtil currentversion') do set currentversion=%%i
set tag=$tagprefix$%currentversion%
set message=Set $messageprefix$version to %currentversion%; pushed to NuGet.
