rem build the documentation
pushd .
cd ..\..\SimpleSyndicate.Help
msbuild /m /nologo /verbosity:quiet /p:Configuration=Release
rmdir /s /q SimpleSyndicate.Help\Help\fti
rmdir /s /q SimpleSyndicate.Help\Help\html
rmdir /s /q SimpleSyndicate.Help\Help\icons
rmdir /s /q SimpleSyndicate.Help\Help\scripts
rmdir /s /q SimpleSyndicate.Help\Help\styles
rmdir /s /q SimpleSyndicate.Help\Help\toc
popd
