rem commit the changes for this version
pushd .
cd ..
git add --all *
git commit -a -m "%message%"
git tag -a %tag% -m "%message%"
git push --all
git push --tags
popd
