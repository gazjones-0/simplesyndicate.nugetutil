rem commit the updated documentation solution
pushd .
cd ..\..\SimpleSyndicate.Help
git add --all *
git commit -a -m "%helpmessage%"
git tag -a %helptag% -m "%helpmessage%"
git push --all
git push --tags
popd
