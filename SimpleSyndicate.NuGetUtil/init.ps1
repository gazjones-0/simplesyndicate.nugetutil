param($installPath, $toolsPath, $package, $project)

# destination details
$destinationDirectoryName = "tools"
$destinationParentDirectory = Join-Path $installPath "..\..\"
$destinationDirectory = Join-Path $destinationParentDirectory $destinationDirectoryName
$destinationName = "SimpleSyndicate.NuGetUtil.exe"
$destination = Join-Path $destinationDirectory $destinationName

# source details
$source = Join-Path $toolsPath $destinationName

# create destination directory if it doesn't exist
$destinationDirectoryExists = Test-Path $destinationDirectory
if (-not $destinationDirectoryExists) {
	New-Item -path $destinationParentDirectory -name $destinationDirectoryName -itemtype directory
}

# remove existing executable if it exists
$destinationExists = Test-Path $destination
if ($destinationExists) {
	Remove-Item $destination -force
}

# copy executable to destination
Copy-Item $source -destination $destination -force
