﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace SimpleSyndicate.NuGetUtil
{
	class Program
	{
		private static bool commandExecuted = false;

		static void Main(string[] args)
		{
			Commands.NoCommand.Execute(args);

			Commands.CheckVersionUpdateArg.CheckVersionUpdateArgCommand.Execute(args);
			Commands.CurrentVersion.CurrentVersionCommand.Execute(args);
			Commands.DependenciesUpdate.DependenciesUpdateCommand.Execute(args);
			Commands.Help.HelpCommand.Execute(args);
			Commands.Init.InitCommand.Execute(args);
			Commands.MultipleReplace.MultipleReplaceCommand.Execute(args);
			Commands.PrependLine.PrependLineCommand.Execute(args);
			Commands.Replace.ReplaceCommand.Execute(args);
			Commands.Version.VersionCommand.Execute(args);
			Commands.VersionUpdate.VersionUpdateCommand.Execute(args);

			// check for invalid command last as it's based on whether a command has been executed
			Commands.InvalidCommand.Execute(args);
		}

		public static void CommandExecuted()
		{
			commandExecuted = true;
		}

		public static bool HasCommandBeenExecuted()
		{
			return commandExecuted;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "System.Console.WriteLine(System.String)")]
		public static void OutputError([Localizable(false)] string message)
		{
			while (message.EndsWith("\r\n", StringComparison.OrdinalIgnoreCase))
			{
				message = message.Substring(0, message.Length - "\r\n".Length);
			}
			Console.WriteLine("Fatal: " + message);
		}

		public static void OutputInfo([Localizable(false)] string message)
		{
			while (message.EndsWith("\r\n", StringComparison.OrdinalIgnoreCase))
			{
				message = message.Substring(0, message.Length - "\r\n".Length);
			}
			Console.WriteLine(message);
		}

		public static void OutputUsage()
		{
			var readme = new List<string>(Properties.Resources.README.Split('\n'));
			while (!readme[0].StartsWith("Usage:", StringComparison.OrdinalIgnoreCase))
			{
				readme.RemoveAt(0);
			}
			while (String.IsNullOrWhiteSpace(readme[readme.Count - 1]) || String.Compare(readme[readme.Count - 1].Trim(), "```", StringComparison.OrdinalIgnoreCase) == 0)
			{
				readme.RemoveAt(readme.Count - 1);
			}
			Console.WriteLine(String.Join("\n", readme));
		}
	}
}
