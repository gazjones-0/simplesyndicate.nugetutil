@echo off
rem check nugetutil is available
if not exist ..\tools\SimpleSyndicate.NuGetUtil.exe (
	echo Fatal: ..\tools\SimpleSyndicate.NuGetUtil.exe not found
	goto :end
)

rem check input argument
for /F "delims=" %%i in ('..\tools\SimpleSyndicate.NuGetUtil.exe checkversionupdatearg %1') do set nugetutil=%%i
set nugetutilprefix=%nugetutil:~0,5%
if /i "%nugetutilprefix%" == "Valid" (
	goto :precheck
)
echo Fatal: No version update component specified
echo Usage: nuget-push ^<major^|minor^|point^|prerelease^>
goto :end

:precheck
echo Release checklist:
echo 1. Removed and sorted usings?
echo 2. Performed code analysis?
choice /m "Checklist completed? [Y]es or [N]o?"
if errorlevel 2 goto :end

:checkTools
rem check Visual Studio is available
if not defined VSINSTALLDIR (
	echo Fatal: Visual Studio not found
	goto :end
)

rem check nuget is available
for /F "delims=" %%i in ('nuget') do set nugetversion=%%i
if not defined nugetversion (
	echo Fatal: nuget not found
	goto :end
)

rem check git is available
for /F "delims=" %%i in ('git --version') do set gitversion=%%i
if not defined gitversion (
	echo Fatal: git not found
	goto :end
)

rem if %home% isn't defined, set it to the home drive and path
if not defined HOME (
	set HOME=%HOMEDRIVE%%HOMEPATH%
)

rem if the home drive and path aren't defined, home will still be undefined so set it to the user profile
if not defined HOME (
	set HOME=%USERPROFILE%
)

rem sanity check %home% is defined, otherwise git won't be able to find its global config
if not defined HOME (
	echo Fatal: No HOME environment variable defined
	echo Fatal: Without this git won't be able to find its global config
	goto :end
)

:push
rem update version
..\tools\SimpleSyndicate.NuGetUtil versionupdatenoreleasenotes %1

rem build the solution
pushd .
cd ..
msbuild /m /nologo /verbosity:quiet /p:Configuration=Release
popd

rem re-build it as a post-build event embeds the output of the last build into itself
rem so a second build ensures that the latest version is embedded in the output
pushd .
cd ..
msbuild /m /nologo /verbosity:quiet /p:Configuration=Release
popd

rem create a sub-folder to perform the packaging as we want to customise the files in it
pushd .
mkdir .nuget
copy *.nuspec .nuget
cd .nuget

rem build release package and push to nuget
nuget pack SimpleSyndicate.NuGetUtil.nuspec
nuget push *.nupkg

rem back to original directory
popd

rem clean up packaging folder
rmdir /s /q .nuget

rem work out the git tags and messages
for /F "delims=" %%i in ('..\tools\SimpleSyndicate.NuGetUtil currentversion') do set currentversion=%%i
set tag=%currentversion%
set message=Set version to %currentversion%; pushed to NuGet.
set othermessage=Updated tools/SimpleSyndicate.NuGetUtil to %currentversion%.

rem commit the changes for this version
pushd .
cd ..
git add --all *
git commit -a -m "%message%"
git tag -a %tag% -m "%message%"
git push --all
git push --tags
popd

rem copy to all the SimpleSyndicate packages

rem SimpleSyndicate
echo.
echo SimpleSyndicate
if not exist ..\..\SimpleSyndicate\tools (
	mkdir ..\..\SimpleSyndicate\tools
)
xcopy ..\tools\SimpleSyndicate.NuGetUtil.exe ..\..\SimpleSyndicate\tools /y
pushd .
cd ..\..\SimpleSyndicate
git add --all tools\*
git commit -a -m "%othermessage%"
git push --all
popd

rem SimpleSyndicate.DependencyInjection.SimpleInjector
echo.
echo SimpleSyndicate.DependencyInjection.SimpleInjector
if not exist ..\..\SimpleSyndicate.DependencyInjection.SimpleInjector\tools (
	mkdir ..\..\SimpleSyndicate.DependencyInjection.SimpleInjector\tools
)
xcopy ..\tools\SimpleSyndicate.NuGetUtil.exe ..\..\SimpleSyndicate.DependencyInjection.SimpleInjector\tools /y
pushd .
cd ..\..\SimpleSyndicate.DependencyInjection.SimpleInjector
git add --all tools\*
git commit -a -m "%othermessage%"
git push --all
popd

rem SimpleSyndicate.Help
echo.
echo SimpleSyndicate.Help
if not exist ..\..\SimpleSyndicate.Help\tools (
	mkdir ..\..\SimpleSyndicate.Help\tools
)
xcopy ..\tools\SimpleSyndicate.NuGetUtil.exe ..\..\SimpleSyndicate.Help\tools /y
pushd .
cd ..\..\SimpleSyndicate.Help
git add --all tools\*
git commit -a -m "%othermessage%"
git push --all
popd

rem SimpleSyndicate.Mvc
echo.
echo SimpleSyndicate.Mvc
if not exist ..\..\SimpleSyndicate.Mvc\tools (
	mkdir ..\..\SimpleSyndicate.Mvc\tools
)
xcopy ..\tools\SimpleSyndicate.NuGetUtil.exe ..\..\SimpleSyndicate.Mvc\tools /y
pushd .
cd ..\..\SimpleSyndicate.Mvc
git add --all tools\*
git commit -a -m "%othermessage%"
git push --all
popd

rem SimpleSyndicate.Mvc.DisplayTemplates
echo.
echo SimpleSyndicate.Mvc.DisplayTemplates
if not exist ..\..\SimpleSyndicate.Mvc.DisplayTemplates\tools (
	mkdir ..\..\SimpleSyndicate.Mvc.DisplayTemplates\tools
)
xcopy ..\tools\SimpleSyndicate.NuGetUtil.exe ..\..\SimpleSyndicate.Mvc.DisplayTemplates\tools /y
pushd .
cd ..\..\SimpleSyndicate.Mvc.DisplayTemplates
git add --all tools\*
git commit -a -m "%othermessage%"
git push --all
popd

rem SimpleSyndicate.Mvc.EditorTemplates
echo.
echo SimpleSyndicate.Mvc.EditorTemplates
if not exist ..\..\SimpleSyndicate.Mvc.EditorTemplates\tools (
	mkdir ..\..\SimpleSyndicate.Mvc.EditorTemplates\tools
)
xcopy ..\tools\SimpleSyndicate.NuGetUtil.exe ..\..\SimpleSyndicate.Mvc.EditorTemplates\tools /y
pushd .
cd ..\..\SimpleSyndicate.Mvc.EditorTemplates
git add --all tools\*
git commit -a -m "%othermessage%"
git push --all
popd

rem SimpleSyndicate.Mvc.ImprovedIdentity
echo.
echo SimpleSyndicate.Mvc.ImprovedIdentity
if not exist ..\..\SimpleSyndicate.Mvc.ImprovedIdentity\tools (
	mkdir ..\..\SimpleSyndicate.Mvc.ImprovedIdentity\tools
)
xcopy ..\tools\SimpleSyndicate.NuGetUtil.exe ..\..\SimpleSyndicate.Mvc.ImprovedIdentity\tools /y
pushd .
cd ..\..\SimpleSyndicate.Mvc.ImprovedIdentity
git add --all tools\*
git commit -a -m "%othermessage%"
git push --all
popd

rem SimpleSyndicate.Mvc.VersionHistoryController
echo.
echo SimpleSyndicate.Mvc.VersionHistoryController
if not exist ..\..\SimpleSyndicate.Mvc.VersionHistoryController\tools (
	mkdir ..\..\SimpleSyndicate.Mvc.VersionHistoryController\tools
)
xcopy ..\tools\SimpleSyndicate.NuGetUtil.exe ..\..\SimpleSyndicate.Mvc.VersionHistoryController\tools /y
pushd .
cd ..\..\SimpleSyndicate.Mvc.VersionHistoryController
git add --all tools\*
git commit -a -m "%othermessage%"
git push --all
popd

rem SimpleSyndicate.Testing
echo.
echo SimpleSyndicate.Testing
if not exist ..\..\SimpleSyndicate.Testing\tools (
	mkdir ..\..\SimpleSyndicate.Testing\tools
)
xcopy ..\tools\SimpleSyndicate.NuGetUtil.exe ..\..\SimpleSyndicate.Testing\tools /y
pushd .
cd ..\..\SimpleSyndicate.Testing
git add --all tools\*
git commit -a -m "%othermessage%"
git push --all
popd

rem SimpleSyndicate.Testing.Mvc
echo.
echo SimpleSyndicate.Testing.Mvc
if not exist ..\..\SimpleSyndicate.Testing.Mvc\tools (
	mkdir ..\..\SimpleSyndicate.Testing.Mvc\tools
)
xcopy ..\tools\SimpleSyndicate.NuGetUtil.exe ..\..\SimpleSyndicate.Testing.Mvc\tools /y
pushd .
cd ..\..\SimpleSyndicate.Testing.Mvc
git add --all tools\*
git commit -a -m "%othermessage%"
git push --all
popd

rem SimpleSyndicate.Utilities
echo.
echo SimpleSyndicate.Utilities
if not exist ..\..\SimpleSyndicate.Utilities\SimpleSyndicate.TfsUtil\tools (
	mkdir ..\..\SimpleSyndicate.Utilities\SimpleSyndicate.TfsUtil\tools
)
xcopy ..\tools\SimpleSyndicate.NuGetUtil.exe ..\..\SimpleSyndicate.Utilities\SimpleSyndicate.TfsUtil\tools /y
pushd .
cd ..\..\SimpleSyndicate.Utilities
git add --all tools\*
git commit -a -m "%othermessage%"
git push --all
popd

rem SimpleSyndicate.Web
echo.
echo SimpleSyndicate.Web
if not exist ..\..\SimpleSyndicate.Web\tools (
	mkdir ..\..\SimpleSyndicate.Web\tools
)
xcopy ..\tools\SimpleSyndicate.NuGetUtil.exe ..\..\SimpleSyndicate.Web\tools /y
pushd .
cd ..\..\SimpleSyndicate.Web
git add --all tools\*
git commit -a -m "%othermessage%"
git push --all
popd

rem add a blank line just so it's easier to read
echo.

:end
